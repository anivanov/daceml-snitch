from time import time
import os
import subprocess
from pathlib import Path
import glob
import re


# DOCKER = os.environ.get("DOCKER", "podman")
# DOCKER_OPTS = os.environ.get("DOCKER_OPTS", "--security-opt label=disable --rm --cap-add=SYS_PTRACE")
# DOCKER_IMG = os.environ.get("DOCKER_IMG", "ghcr.io/pulp-platform/snitch@sha256:f43d2db7c97bdcd653deb567664032940e8d9051a80a52c22f239e19fe4c310b")
REPO_ROOT = Path(__file__).parent.parent.resolve()
CWD = Path.cwd()
# IMAGE_CMD = f"{DOCKER} run {DOCKER_OPTS} --mount type=bind,source={REPO_ROOT},target=/repo --mount type=bind,source={CWD},target=/work -w /work {DOCKER_IMG}".split()


def compile_and_run_snitch_code(sources, bin_src, include_dirs=[], simulator="banshee"):
    
    print("Compiling...")
    t1 = time()
    compile_snitch_code(sources, bin_src, include_dirs=include_dirs, toolchain='gcc', target=simulator)
    t2 = time()
    print(f"Compiling... DONE ({t2-t1:.3f} s)")

    if simulator == "banshee":
        run_cmd = f"RUST_MIN_STACK=134217728 SNITCH_LOG= banshee --configuration /repo/snitch_cluster.yaml --latency {bin_src}"
        # warning: do not split run_cmd as it will break the command
        # full_run_cmd = IMAGE_CMD + ['/bin/bash', '-c', run_cmd]
        full_run_cmd = ['/bin/bash', '-c', run_cmd]
    elif simulator == "rtl":
        # NUMACTL_PREFIX = os.getenv("NUMACTL_PREFIX", "") # numactl -m 0 -C 0-15
        run_cmd = f"/repo/snitch_cluster.vlt {bin_src}"
        # option 1: run in container (slight performance overhead, numactl not available)
        #full_run_cmd = IMAGE_CMD + ['/bin/bash', '-c', run_cmd]
        # option 2: run outside of container (potentially may not work on all systems)
        # full_run_cmd = f"{NUMACTL_PREFIX} {REPO_ROOT}/snitch_cluster.vlt {bin_src}".split()
        full_run_cmd = ['/bin/bash', '-c', run_cmd]

    print(f"Running ({simulator})...")
    t1 = time()
    result = subprocess.run(
        full_run_cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        universal_newlines=True
    )
    t2 = time()
    print(f"Running ({simulator})... DONE ({t2-t1:.3f} s)")
    
    if result.returncode != 0:
        print(result.stdout)
    
    # process logs
    if simulator == "rtl":
        print(f"Postprocessing logs...")
        logs = glob.glob('logs/trace_hart_*.dasm')
        if not logs:
            raise Exception("Could not find trace logs")
        t1 = time()
        for dasm in logs:
            txt = dasm.replace('dasm', 'txt')
            trace = dasm.replace('dasm', 'trace')
            run_and_check(['/bin/bash', '-c', f"spike-dasm < {dasm} > {txt}"])
            run_and_check(['/bin/bash', '-c', f"/repo/snitch/util/gen_trace.py < {txt} > {trace}"])
        t2 = time()
        print(f"Postprocessing logs... DONE ({t2-t1:.3f} s)")
    if result.returncode != 0:
        raise Exception(f"Simulation failed: {' '.join(full_run_cmd)}")

    # find patter with regex: "Cycles: 1476"
    m = re.search(r"Cycles: (\d+)", result.stdout)
    if m is None:
        raise Exception("Could not find cycle count")
    cycles = int(m.group(1))
    
    return cycles


def run_and_check(cmd, failure="Command failed"):
    result = subprocess.run(
        cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        universal_newlines=True
    )
    if result.returncode != 0:
        print(result.stdout)
        raise Exception(f"{failure}: {' '.join(cmd)}")
    return result


def compile_snitch_code(sources, binary_name, include_dirs, toolchain, target):
    target_str = {
        "banshee": "banshee",
        "rtl": "cluster",
    }[target]
    
    if toolchain == 'llvm':
        # clang toolchain for snitch (deprecated in code generation pipeline due to lack of SSR register protection)
        CC = "/tools/riscv-llvm/bin/clang"
        CXX = "/tools/riscv-llvm/bin/clang++"
        CFLAGS = ' '.join([
            "-g", "-O3",
            "-mcpu=snitch", "-mcmodel=medany", "-ffast-math",
            "-flto",
            "-fno-builtin-printf", "-fno-common", "-ffunction-sections",
            "-static", "-mllvm", "-enable-misched=false", "-mno-relax",
            "-fopenmp", "-menable-experimental-extensions",
            "-isystem", "/tools/riscv-llvm/riscv32-unknown-elf/include/",
            "-isystem", "/tools/riscv-llvm/lib/clang/12.0.1/include/",
            "-isystem", "/repo/snitch/sw/snRuntime/vendor/",
            "-isystem", "/repo/snitch/sw/snRuntime/include/",
            "-isystem", "/repo/snitch/sw/snRuntime/vendor/riscv-opcodes/",
            # "-I", "/repo/dnn_kernels/microkernels/",  # snrt_mini.h
        ] + [f"-I{d}" for d in include_dirs])
        LDFLAGS = ' '.join([
            "-flto",
            "-mcpu=snitch", "-nostartfiles", "-fuse-ld=lld", "-Wl,--image-base=0x80000000",
            "-nostdlib",
            "-static",
            "-Wl,-z,norelro",
            "-Wl,--gc-sections",
            "-Wl,--no-relax",
            "-nodefaultlibs",
            "-T", "/repo/snRuntime-build/common.ld",
            f"/repo/snRuntime-build/libsnRuntime-{target_str}.a",
            "/tools/riscv-llvm/riscv32-unknown-elf/lib/libc.a",
            "/tools/riscv-llvm/riscv32-unknown-elf/lib/libm.a",
            "/tools/riscv-llvm/lib/clang/12.0.1/lib/libclang_rt.builtins-riscv32.a",
        ])
    elif toolchain == 'gcc':
        

        CC = "/tools/riscv/bin/riscv64-unknown-elf-gcc"
        CXX = "/tools/riscv/bin/riscv64-unknown-elf-g++"
        CFLAGS = ' '.join([
            "-g", "-O3",
            "-flto",
            "-march=rv32imafd", "-mabi=ilp32d", "-mcmodel=medany", "-ffast-math", "-fno-builtin-printf", "-fno-common",
            "-nostdinc",
            "-ffixed-ft0", "-ffixed-ft1", "-ffixed-ft2",
            "-fmax-errors=10",
            "-isystem", "/tools/riscv/riscv64-unknown-elf/include/",
            "-isystem", "/tools/riscv/lib/gcc/riscv64-unknown-elf/8.3.0/include/",
            "-isystem", "/repo/snitch/sw/snRuntime/vendor/",
            "-isystem", "/repo/snitch/sw/snRuntime/include/",
            "-isystem", "/repo/snitch/sw/snRuntime/vendor/riscv-opcodes/",
            "-I", "/repo/samples/",  # dace_mini.h
        ] + [f"-I{d}" for d in include_dirs])
        LDFLAGS = ' '.join([
            "-flto",
            "-march=rv32imafd", "-mabi=ilp32d", "-nostartfiles", "-Wl,-Ttext-segment=0x80000000",
            "-nodefaultlibs", "-ffunction-sections",
            "-T", "/repo/snRuntime-gcc-build/common.ld",
            f"/repo/snRuntime-gcc-build/libsnRuntime-{target_str}.a",
            "/tools/riscv/riscv64-unknown-elf/lib/rv32imfd/ilp32d/libc.a",
            "/tools/riscv/riscv64-unknown-elf/lib/rv32imfd/ilp32d/libm.a",
            "/tools/riscv/lib/gcc/riscv64-unknown-elf/8.3.0/rv32imfd/ilp32d/libgcc.a",
            "-lc",
            "/repo/gcc_div.a",
        ])
    else:
        assert 0
    
    # we use clang for objdump (irrespective of build toolchain) as it is able to decode snitch extensions
    OBJDUMP = "/tools/riscv-llvm/bin/llvm-objdump --mcpu=snitch"
    # BUILD_DIR = "/work/build"
    # AR = "/tools/riscv-llvm/bin/llvm-ar"
    # ARFLAGS = "rv"
    # RANLIB = "/tools/riscv-llvm/bin/llvm-ranlib"

    # object files:
    # $(CC) $(CFLAGS) -c -o foo.o foo.c
    # executable:
    # $(CC) $(LDFLAGS) foo.o -o foo
    # executable objdump
    # $(OBJDUMP) -dhS foo > foo.s
    # static library:
    # $(AR) $(ARFLAGS) libfoo.a foo.o

    for src in sources:
        COMP = CC if src.suffix == 'c' else CXX
        # build object file
        obj_o = src.with_suffix('.o')
        src_cmd = f"{COMP} {CFLAGS} -c -o {obj_o} {src}"
        run_and_check(src_cmd.split())
        # run_and_check(IMAGE_CMD + src_cmd.split())

    objects = [str(src.with_suffix('.o')) for src in sources]
    all_obj_o = ' '.join(objects)

    if binary_name:
        # link into executable
        # LDFLAGS should come after object files as they may define symbols used in object files
        exe_cmd = f"{CXX} {CFLAGS} {all_obj_o} {LDFLAGS} -o {binary_name}"
        # cmd = IMAGE_CMD + exe_cmd.split()
        cmd = exe_cmd.split()
        run_and_check(cmd)

        # objdump
        objdump_cmd = f"{OBJDUMP} -dhS {binary_name} > {binary_name}.s"
        # cmd = IMAGE_CMD + ['/bin/bash', '-c', objdump_cmd]
        cmd = ['/bin/bash', '-c', objdump_cmd]
        run_and_check(cmd)
