import torch
import onnx
from daceml.onnx import ONNXModel
from simulation import run_onnx, run 
import argparse


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--simulator", type=str, default='banshee')
    parser.add_argument("--toolchain", type=str, default='llvm')
    args = parser.parse_args()
    
    model = torch.hub.load('ultralytics/yolov5', 'yolov5n', pretrained=True)
    img = torch.zeros(1, 3, 640, 640)
    torch.onnx.export(
        model,
        img,
        "yolov5n.onnx",
        verbose=False,
        training=torch.onnx.TrainingMode.EVAL,
        opset_version=12,
        export_params=True,
        do_constant_folding=False,
        keep_initializers_as_inputs=True)

    dace_model = ONNXModel("yolov5n", onnx.load("yolov5n.onnx"))
    output = dace_model(img)
    
    run(dace_model, [img], toolchain=args.toolchain, simulator=args.simulator,  verify=False, opts='-O3 -g')
