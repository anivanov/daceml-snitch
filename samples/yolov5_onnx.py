import torch
import onnx
from daceml.onnx import ONNXModel
from simulation import run, run_onnx
import argparse
import sys


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--simulator", type=str, default='banshee')
    parser.add_argument("--toolchain", type=str, default='llvm')
    args = parser.parse_args()
    
    img = torch.zeros(1, 3, 640, 640)

    dace_model = ONNXModel("yolov5s", onnx.load("samples/yolov5s.onnx"))
    output = dace_model(img)
    
    run(dace_model, [img], toolchain=args.toolchain, simulator=args.simulator, verify=False, opts='-O3 -g')
