import torch
from daceml.torch import DaceModule
from simulation import run
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--simulator", type=str, default='banshee')
    parser.add_argument("--toolchain", type=str, default='llvm')
    args = parser.parse_args()
    
    feature = 4
    head = 2
    batch = 3
    seq = 5
    feedforward = 7

    # feature = 2
    # head = 1
    # batch = 2
    # seq = 2
    # feedforward = 2

    encoder_layer = torch.nn.TransformerEncoderLayer(d_model=feature, nhead=head, dim_feedforward=feedforward)
    input = torch.rand(seq, batch, feature) 
    dm = DaceModule(encoder_layer)
    
    run(dm, [input], toolchain=args.toolchain, simulator=args.simulator, opts='-O3 -g')
