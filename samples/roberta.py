import torch
import transformers
import numpy
import datasets
import pandas
import sklearn
import onnx
import onnxruntime as ort
import sys
from daceml.torch import DaceModule
from simulation import run
from onnxsim import simplify
import argparse


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--simulator", type=str, default='banshee')
    parser.add_argument("--toolchain", type=str, default='llvm')
    args = parser.parse_args()
    
print("torch version:", torch.__version__)
print("transformers version:", transformers.__version__)
print("datasets version:", datasets.__version__)
print("numpy version:", numpy.__version__)
print("pandas version:", pandas.__version__)
print("scikit-learn version:", sklearn.__version__)
print("onnx version:", onnx.__version__)
print("onnxruntime:",ort.__version__)
import torch.onnx
import onnx
from transformers import RobertaTokenizer, RobertaForMaskedLM, RobertaConfig
from daceml.onnx import ONNXModel
import os

# Initialize tokenizer and model with configuration
# tokenizer_path = os.path.join('/smile/saved_model', 'tokenizer')
# tokenizer = RobertaTokenizer.from_pretrained(tokenizer_path)
tokenizer = RobertaTokenizer.from_pretrained('seyonec/SMILES_tokenized_PubChem_shard00_160k')
config_path = 'seyonec/SMILES_tokenized_PubChem_shard00_160k'
config = RobertaConfig.from_pretrained(config_path)
model = RobertaForMaskedLM(config=config)
model.classifier = torch.nn.Linear(config.hidden_size, 1)

# Load the model weights
# model_path = '/smile/saved_model/fine_tuned_model.pt'
device = torch.device("cpu")
# model.load_state_dict(torch.load(model_path, map_location=device))
model.eval()  # Ensure the model is in evaluation mode

# Define the SMILES string and tokenize
smiles = 'C1(=C2C(C=CC=C2)=NC=N1)NC3=CC(OC)=CC=C3'
inputs = tokenizer(smiles, return_tensors="pt", padding='max_length', max_length=512, truncation=True)
inputs = {key: tensor.to(device) for key, tensor in inputs.items()}
dace_model = DaceModule(model, cuda=False, backward=False)

input_ids = inputs['input_ids']
attention_mask = inputs['attention_mask']
torch.onnx.export(
    model,
    (input_ids, attention_mask),  # If both are actually used in the model's forward pass
    "model.onnx",
    export_params=True,
    opset_version=12,
    do_constant_folding=True,
    input_names=['input_ids', 'attention_mask'],  # Ensure these names match model's forward definition
    output_names=['output'],
    dynamic_axes={'input_ids': {0: 'batch_size'}, 'attention_mask': {0: 'batch_size'}},
    training=torch.onnx.TrainingMode.EVAL
)

output = run(dace_model, (input_ids, attention_mask), toolchain=args.toolchain, simulator=args.simulator, verify=False, opts='-O3 -g')

exit(0)

input_ids = inputs['input_ids']
# print(f"input_ids",input_ids)
attention_mask = inputs['attention_mask']
token_type_ids = inputs.get('token_type_ids', torch.zeros_like(input_ids))  # Ensure compatibility

# Export the model to ONNX
# onnx_model_path = '/smile/saved_model/roberta_smiles.onnx'
onnx_model_path = "model.onnx"

torch.onnx.export(
    model,
    (input_ids, attention_mask),  # If both are actually used in the model's forward pass
    "model.onnx",
    export_params=True,
    opset_version=12,
    do_constant_folding=True,
    input_names=['input_ids', 'attention_mask'],  # Ensure these names match model's forward definition
    output_names=['output'],
    dynamic_axes={'input_ids': {0: 'batch_size'}, 'attention_mask': {0: 'batch_size'}},
    training=torch.onnx.TrainingMode.EVAL
)

        # Load the ONNX model and prepare it for the simulator
dace_model = ONNXModel("roberta_smiles", onnx.load(onnx_model_path))

    # Run the model using DaCeML with the Banshee simulator
output = run(dace_model, [inputs], toolchain=args.toolchain, simulator=args.simulator, verify=False, opts='-O3 -g')
    
#print("Model output:", output)
model_onnx = onnx.load(onnx_model_path)
model_simplified, check = simplify(model_onnx,skip_shape_inference=True,skip_fuse_bn=True,perform_optimization=False)

#model_simplified, check = simplify(model_onnx)
#model_simplified, check = simplify(model_onnx,
#skip_fuse_bn=True,
#input_shapes={'input_ids': input_ids.shape,
#'attention_mask': attention_mask.shape,
#'token_type_ids': token_type_ids.shape})

 # Check if the simplification was successful
#if check:
#     print("Simplification was successful.")
#     onnx.save(model_simplified, "simplified_roberta_smiles.onnx")
#else:
#     print("Simplification may have failed or not been effective.")

dace_model = ONNXModel("simplified_roberta_smiles", onnx.load(onnx_model_path))
# dace_model = ONNXModel("simplified_roberta_smiles", onnx.load("simplified_roberta_smiles.onnx"))
print("Max input_id:", input_ids.max().item())
print("Max token_type_id:", token_type_ids.max().item())
print("Vocabulary size:", model.config.vocab_size)
inputs = {key: tensor.to(device) for key, tensor in inputs.items()}
dace_output = model(**inputs)

session=ort.InferenceSession(onnx_model_path) 

#output=session.run(None, input_ids,attention_mask)
print(type(inputs),inputs) 
#output=session.run(None, inputs)

# Your current inputs are PyTorch tensors; convert them to NumPy arrays
#inputs_numpy = {key: value.detach().cpu().numpy() for key, value in inputs.items()}

input_ids_numpy_array = inputs['input_ids'].detach().cpu().numpy()
inputs_numpy = {'input_ids': input_ids_numpy_array}

# Run the session
# output_names = [output.name for output in session.get_outputs()]
# outputs = session.run(output_names, inputs_numpy)
# predicted_score = outputs[0].item
 # Adjust the call to use dictionary unpacking for the model
#output = run(dace_model, [inputs], toolchain=args.toolchain, simulator=args.simulator, opts='-O3 -g')

# print("Predicted output:", predicted_score)
