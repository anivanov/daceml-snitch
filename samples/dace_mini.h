#pragma once

#include "snrt.h"
#include "math.h"

// from types.h
#define DACE_EXPORTED extern "C"
#define DACE_CONSTEXPR
#define DACE_HDFI inline
#define __DACE_UNROLL

namespace dace
{
    typedef bool bool_;
    typedef int8_t  int8;
    typedef int16_t int16;
    typedef int32_t int32;
    typedef int64_t int64;
    typedef uint8_t  uint8;
    typedef uint16_t uint16;
    typedef uint32_t uint32;
    typedef uint64_t uint64;
    typedef float float32;
    typedef double float64;

    enum class ReductionType {
        Custom = 0,
        Min = 1,
        Max = 2,
        Sum = 3,
        Product = 4,
        Logical_And = 5,
        Bitwise_And = 6,
        Logical_Or = 7,
        Bitwise_Or = 8,
        Logical_Xor = 9,
        Bitwise_Xor = 10,
        Min_Location = 11,
        Max_Location = 12,
        Exchange = 13
    };
}

// from math.h

template <typename T>
DACE_CONSTEXPR DACE_HDFI T min(const T& val)
{
    return val;
}
template <typename T, typename... Ts>
DACE_CONSTEXPR DACE_HDFI T min(const T& a, const Ts&... ts)
{
    return (a < min(ts...)) ? a : min(ts...);
}

template <typename T>
DACE_CONSTEXPR DACE_HDFI T max(const T& val)
{
    return val;
}
template <typename T, typename... Ts>
DACE_CONSTEXPR DACE_HDFI T max(const T& a, const Ts&... ts)
{
    return (a > max(ts...)) ? a : max(ts...);
}

template <typename T, typename... Ts>
DACE_CONSTEXPR DACE_HDFI T Max(const T& a, const Ts&... ts)
{
    return (a > max(ts...)) ? a : max(ts...);
}

template <typename T, typename... Ts>
DACE_CONSTEXPR DACE_HDFI T Min(const T& a, const Ts&... ts)
{
    return (a < min(ts...)) ? a : min(ts...);
}


namespace dace
{
    namespace math
    {
        template<typename T>
        DACE_CONSTEXPR DACE_HDFI T sqrt(const T& a)
        {
            return ::sqrt(a);
        }

        template<typename T>
        DACE_CONSTEXPR DACE_HDFI T exp(const T& a)
        {
            return ::exp(a);
        }

        template<typename T>
        DACE_CONSTEXPR DACE_HDFI T pow(const T& a, const T& b)
        {
            return ::pow(a, b);
        }
    }
}

//  from dace/dace/runtime/include/dace/reduction.h
namespace dace {

    // Internal type. See below for wcr_fixed external type, which selects
    // the implementation according to T's properties.
    template <ReductionType REDTYPE, typename T>
    struct _wcr_fixed
    {
        static DACE_HDFI T reduce_atomic(T *ptr, const T& value);

        DACE_HDFI T operator()(const T &a, const T &b) const;
    };


    // Custom reduction with a lambda function
    template <typename T>
    struct wcr_custom {
        template <typename WCR>
        static DACE_HDFI T reduce_atomic(WCR wcr, T *ptr, const T& value) {
            // The slowest kind of atomic operations (locked/compare-and-swap),
            // this should only happen in case of unrecognized lambdas
            T old;
            old = *ptr;
            *ptr = wcr(old, value);
            return old;
        }

        // Non-conflicting version --> no critical section
        template <typename WCR>
        static DACE_HDFI T reduce(WCR wcr, T *ptr, const T& value) {
            T old = *ptr;
            *ptr = wcr(old, value);
            return old;
        }
    };

    // Specialization of CAS for float and double
    template <>
    struct wcr_custom<float> {
        template <typename WCR>
        static DACE_HDFI float reduce_atomic(WCR wcr, float *ptr, const float& value) {
            // The slowest kind of atomic operations (locked/compare-and-swap),
            // this should only happen in case of unrecognized lambdas
            float old;
            {
                old = *ptr;
                *ptr = wcr(old, value);
            }
            return old;
        }

        // Non-conflicting version --> no critical section
        template <typename WCR>
        static DACE_HDFI float reduce(WCR wcr, float *ptr, const float& value) {
            float old = *ptr;
            *ptr = wcr(old, value);
            return old;
        }
    };

    template <>
    struct wcr_custom<double> {
        template <typename WCR>
        static DACE_HDFI double reduce_atomic(WCR wcr, double *ptr, const double& value) {
            // The slowest kind of atomic operations (locked/compare-and-swap),
            // this should only happen in case of unrecognized lambdas
            double old;
            {
                old = *ptr;
                *ptr = wcr(old, value);
            }
            return old;
        }

        // Non-conflicting version --> no critical section
        template <typename WCR>
        static DACE_HDFI double reduce(WCR wcr, double *ptr, const double& value) {
            double old = *ptr;
            *ptr = wcr(old, value);
            return old;
        }
    };
    // End of specialization

    template <typename T>
    struct _wcr_fixed<ReductionType::Sum, T> {
       
        static DACE_HDFI T reduce_atomic(T *ptr, const T& value) {

                *ptr += value;
                return T(0); // Unsupported
        }

        DACE_HDFI T operator()(const T &a, const T &b) const { return a + b; }
    };

    template <typename T>
    struct _wcr_fixed<ReductionType::Product, T> {

        static DACE_HDFI T reduce_atomic(T *ptr, const T& value) { 

                *ptr *= value;
                return T(0); // Unsupported
        }


        DACE_HDFI T operator()(const T &a, const T &b) const { return a * b; }
    };

    template <typename T>
    struct _wcr_fixed<ReductionType::Min, T> {

        static DACE_HDFI T reduce_atomic(T *ptr, const T& value) { 
                return wcr_custom<T>::reduce_atomic(
                    _wcr_fixed<ReductionType::Min, T>(), ptr, value);
        }


        DACE_HDFI T operator()(const T &a, const T &b) const { return ::min(a, b); }
    };
    
    template <typename T>
    struct _wcr_fixed<ReductionType::Max, T> {

        static DACE_HDFI T reduce_atomic(T *ptr, const T& value) { 
                return wcr_custom<T>::reduce_atomic(
                    _wcr_fixed<ReductionType::Max, T>(), ptr, value);
        }


        DACE_HDFI T operator()(const T &a, const T &b) const { return ::max(a, b); }
    };

    // Specialization for floating point types
    template <>
    struct _wcr_fixed<ReductionType::Min, float> {

        static DACE_HDFI float reduce_atomic(float *ptr, const float& value) { 
            return wcr_custom<float>::reduce_atomic(
                _wcr_fixed<ReductionType::Min, float>(), ptr, value);
        }


        DACE_HDFI float operator()(const float &a, const float &b) const { return ::min(a, b); }
    };
    
    template <>
    struct _wcr_fixed<ReductionType::Max, float> {

        static DACE_HDFI float reduce_atomic(float *ptr, const float& value) { 
            return wcr_custom<float>::reduce_atomic(
                _wcr_fixed<ReductionType::Max, float>(), ptr, value);
        }

        DACE_HDFI float operator()(const float &a, const float &b) const { return ::max(a, b); }
    };

    template <>
    struct _wcr_fixed<ReductionType::Min, double> {

        static DACE_HDFI double reduce_atomic(double *ptr, const double& value) { 
            return wcr_custom<double>::reduce_atomic(
                _wcr_fixed<ReductionType::Min, double>(), ptr, value);
        }


        DACE_HDFI double operator()(const double &a, const double &b) const { return ::min(a, b); }
    };
    
    template <>
    struct _wcr_fixed<ReductionType::Max, double> {

        static DACE_HDFI double reduce_atomic(double *ptr, const double& value) { 
            return wcr_custom<double>::reduce_atomic(
                _wcr_fixed<ReductionType::Max, double>(), ptr, value);
        }

        DACE_HDFI double operator()(const double &a, const double &b) const { return ::max(a, b); }
    };
    // End of specialization

    template <typename T>
    struct _wcr_fixed<ReductionType::Logical_And, T> {

        static DACE_HDFI T reduce_atomic(T *ptr, const T& value) { 
                T val = (value ? T(1) : T(0));
                #pragma omp atomic
                *ptr &= val;
                return T(0); // Unsupported
        }


        DACE_HDFI T operator()(const T &a, const T &b) const { return a && b; }
    };

    template <typename T>
    struct _wcr_fixed<ReductionType::Bitwise_And, T> {

        static DACE_HDFI T reduce_atomic(T *ptr, const T& value) { 
                *ptr &= value;
                return T(0); // Unsupported
        }


        DACE_HDFI T operator()(const T &a, const T &b) const { return a & b; }
    };

    template <typename T>
    struct _wcr_fixed<ReductionType::Logical_Or, T> {

        static DACE_HDFI T reduce_atomic(T *ptr, const T& value) { 
                T val = (value ? T(1) : T(0));
                *ptr |= val;
                return T(0); // Unsupported
        }


        DACE_HDFI T operator()(const T &a, const T &b) const { return a || b; }
    };

    template <typename T>
    struct _wcr_fixed<ReductionType::Bitwise_Or, T> {

        static DACE_HDFI T reduce_atomic(T *ptr, const T& value) { 
                *ptr |= value;
                return T(0); // Unsupported
        }


        DACE_HDFI T operator()(const T &a, const T &b) const { return a | b; }
    };

    template <typename T>
    struct _wcr_fixed<ReductionType::Logical_Xor, T> {

        static DACE_HDFI T reduce_atomic(T *ptr, const T& value) { 
                T val = (value ? T(1) : T(0));
                *ptr ^= val;
                return T(0); // Unsupported
        }


        DACE_HDFI T operator()(const T &a, const T &b) const { return a != b; }
    };

    template <typename T>
    struct _wcr_fixed<ReductionType::Bitwise_Xor, T> {

        static DACE_HDFI T reduce_atomic(T *ptr, const T& value) { 
                *ptr ^= value;
                return T(0); // Unsupported
        }


        DACE_HDFI T operator()(const T &a, const T &b) const { return a ^ b; }
    };

    template <typename T>
    struct _wcr_fixed<ReductionType::Exchange, T> {

        static DACE_HDFI T reduce_atomic(T *ptr, const T& value) { 
                T old;
                {
                    old = *ptr;
                    *ptr = value;
                }
                return old;
        }

        DACE_HDFI T operator()(const T &a, const T &b) const { return b; }
    };

    // Any vector type that is not of length 1, or struct/complex types 
    // do not support atomics. In these cases, we regress to locked updates.
    template <ReductionType REDTYPE, typename T>
    struct wcr_fixed
    {
        static DACE_HDFI T reduce(T *ptr, const T& value)
        {
            T old = *ptr;
            *ptr = _wcr_fixed<REDTYPE, T>()(old, value);
            return old;
        }

        static DACE_HDFI T reduce_atomic(T *ptr, const T& value) 
        {
            return wcr_custom<T>::template reduce_atomic(
                _wcr_fixed<REDTYPE, T>(), ptr, value);
        }
    };

}  // namespace dace


//  from dace/dace/runtime/include/dace/copy.h
namespace dace
{
    template<typename T, typename U>
    inline void InitArray(T *ptr, const U& value, int size)
    {
        for (int i = 0; i < size; ++i)
            *ptr++ = T(value);
    }

    template <typename T, int VECLEN, int ALIGNED, int... COPYDIMS>
    struct CopyND;
    template <typename T, int VECLEN, int ALIGNED, int N>
    struct CopyNDDynamic;

    template <typename T, int VECLEN, int ALIGNED, int COPYDIM,
              int... OTHER_COPYDIMS>
    struct CopyND<T, VECLEN, ALIGNED, COPYDIM, OTHER_COPYDIMS...>
    {
        template <int SRC_STRIDE, int... OTHER_SRCDIMS>
        struct ConstSrc
        {
            template <typename... Args>
            static DACE_HDFI void Copy(const T *src, T *dst, const int& dst_stride, const Args&... dst_otherdims)
            {
                __DACE_UNROLL
                for (int i = 0; i < COPYDIM; ++i)
                    CopyND<T, VECLEN, ALIGNED, OTHER_COPYDIMS...>::template ConstSrc<OTHER_SRCDIMS...>::Copy(
                        src + i * SRC_STRIDE, dst + i * dst_stride, dst_otherdims...);
            }

            template <typename ACCUMULATE, typename... Args>
            static DACE_HDFI void Accumulate(const T *src, T *dst, ACCUMULATE acc, const int& dst_stride, const Args&... dst_otherdims)
            {
                __DACE_UNROLL
                for (int i = 0; i < COPYDIM; ++i)
                    CopyND<T, VECLEN, ALIGNED, OTHER_COPYDIMS...>::template ConstSrc<OTHER_SRCDIMS...>::Accumulate(
                        src + i * SRC_STRIDE, dst + i * dst_stride, acc, dst_otherdims...);
            }

            template <typename ACCUMULATE, typename... Args>
            static DACE_HDFI void Accumulate_atomic(const T *src, T *dst, ACCUMULATE acc, const int& dst_stride, const Args&... dst_otherdims)
            {
                __DACE_UNROLL
                for (int i = 0; i < COPYDIM; ++i)
                    CopyND<T, VECLEN, ALIGNED, OTHER_COPYDIMS...>::template ConstSrc<OTHER_SRCDIMS...>::Accumulate_atomic(
                        src + i * SRC_STRIDE, dst + i * dst_stride, acc, dst_otherdims...);
            }
        };

        template <int DST_STRIDE, int... OTHER_DSTDIMS>
        struct ConstDst
        {
            template <typename... Args>
            static DACE_HDFI void Copy(const T *src, T *dst, const int& src_stride, const Args&... src_otherdims)
            {
                __DACE_UNROLL
                for (int i = 0; i < COPYDIM; ++i)
                    CopyND<T, VECLEN, ALIGNED, OTHER_COPYDIMS...>::template ConstDst<OTHER_DSTDIMS...>::Copy(
                        src + i * src_stride, dst + i * DST_STRIDE, src_otherdims...);
            }
            
            template <typename ACCUMULATE, typename... Args>
            static DACE_HDFI void Accumulate(const T *src, T *dst, ACCUMULATE acc, const int& src_stride, const Args&... src_otherdims)
            {
                __DACE_UNROLL
                for (int i = 0; i < COPYDIM; ++i)
                    CopyND<T, VECLEN, ALIGNED, OTHER_COPYDIMS...>::template ConstDst<OTHER_DSTDIMS...>::Accumulate(
                        src + i * src_stride, dst + i * DST_STRIDE, acc, src_otherdims...);
            }
                        
            template <typename ACCUMULATE, typename... Args>
            static DACE_HDFI void Accumulate_atomic(const T *src, T *dst, ACCUMULATE acc, const int& src_stride, const Args&... src_otherdims)
            {
                __DACE_UNROLL
                for (int i = 0; i < COPYDIM; ++i)
                    CopyND<T, VECLEN, ALIGNED, OTHER_COPYDIMS...>::template ConstDst<OTHER_DSTDIMS...>::Accumulate_atomic(
                        src + i * src_stride, dst + i * DST_STRIDE, acc, src_otherdims...);
            }
        };

        struct Dynamic
        {
            template <typename... Args>
            static DACE_HDFI void Copy(const T *src, T *dst, const int& src_stride, const int& dst_stride, const Args&... otherdims)
            {
                __DACE_UNROLL
                for (int i = 0; i < COPYDIM; ++i)
                    CopyND<T, VECLEN, ALIGNED, OTHER_COPYDIMS...>::Dynamic::Copy(
                        src + i * src_stride, dst + i * dst_stride, otherdims...);
            }

            template <typename ACCUMULATE, typename... Args>
            static DACE_HDFI void Accumulate(const T *src, T *dst, ACCUMULATE acc, const int& src_stride, const int& dst_stride, const Args&... otherdims)
            {
                __DACE_UNROLL
                for (int i = 0; i < COPYDIM; ++i)
                    CopyND<T, VECLEN, ALIGNED, OTHER_COPYDIMS...>::Dynamic::Accumulate(
                        src + i * src_stride, dst + i * dst_stride, acc, otherdims...);
            }
            
            template <typename ACCUMULATE, typename... Args>
            static DACE_HDFI void Accumulate_atomic(const T *src, T *dst, ACCUMULATE acc, const int& src_stride, const int& dst_stride, const Args&... otherdims)
            {
                __DACE_UNROLL
                for (int i = 0; i < COPYDIM; ++i)
                    CopyND<T, VECLEN, ALIGNED, OTHER_COPYDIMS...>::Dynamic::Accumulate_atomic(
                        src + i * src_stride, dst + i * dst_stride, acc, otherdims...);
            }
        };
    };
    
    // Specialization for actual copy / accumulation
    template <typename T, int VECLEN, int ALIGNED>
    struct CopyND<T, VECLEN, ALIGNED>
    {
        template <int...>
        struct ConstSrc
        {
            static DACE_HDFI void Copy(const T *src, T *dst)
            {
                *(T *)dst = *(T *)src;
            }

            template <typename ACCUMULATE, typename... Args>
            static DACE_HDFI void Accumulate(const T *src, T *dst, ACCUMULATE acc)
            {
                *(T *)dst = acc(*(T *)dst, *(T *)src);
            }

            template <typename ACCUMULATE, typename... Args>
            static DACE_HDFI void Accumulate_atomic(const T *src, T *dst, ACCUMULATE acc)
            {
                wcr_custom<T>::reduce_atomic(acc, (T *)dst, *(T *)src);
            }
        };

        template <int...>
        struct ConstDst
        {
            static DACE_HDFI void Copy(const T *src, T *dst)
            {
                *(T *)dst = *(T *)src;
            }

            template <typename ACCUMULATE>
            static DACE_HDFI void Accumulate(const T *src, T *dst, ACCUMULATE acc)
            {
                *(T *)dst = acc(*(T *)dst, *(T *)src);
            }
            
            template <typename ACCUMULATE>
            static DACE_HDFI void Accumulate_atomic(const T *src, T *dst, ACCUMULATE acc)
            {
                wcr_custom<T>::reduce_atomic(acc, (T *)dst, *(T *)src);
            }
        };

        struct Dynamic
        {
            static DACE_HDFI void Copy(const T *src, T *dst)
            {
                *(T *)dst = *(T *)src;
            }

            template <typename ACCUMULATE>
            static DACE_HDFI void Accumulate(const T *src, T *dst, ACCUMULATE acc)
            {
                *(T *)dst = acc(*(T *)dst, *(T *)src);
            }
            
            template <typename ACCUMULATE>
            static DACE_HDFI void Accumulate_atomic(const T *src, T *dst, ACCUMULATE acc)
            {
                wcr_custom<T>::reduce_atomic(acc, (T *)dst, *(T *)src);
            }
        };
    };

    template <typename T, int VECLEN, int ALIGNED, int N>
    struct CopyNDDynamic
    {
        template <int SRC_STRIDE, int... OTHER_SRCDIMS>
        struct ConstSrc
        {
            template <typename... Args>
            static DACE_HDFI void Copy(const T *src, T *dst, const int& copydim, const int& dst_stride, const Args&... otherdims)
            {
                __DACE_UNROLL
                for (int i = 0; i < copydim; ++i)
                    CopyNDDynamic<T, VECLEN, ALIGNED, N-1>::template ConstSrc<OTHER_SRCDIMS...>::Copy(
                        src + i * SRC_STRIDE, dst + i * dst_stride, otherdims...);
            }

            template <typename ACCUMULATE, typename... Args>
            static DACE_HDFI void Accumulate(const T *src, T *dst, ACCUMULATE acc, const int& copydim, const int& dst_stride, const Args&... otherdims)
            {
                __DACE_UNROLL
                for (int i = 0; i < copydim; ++i)
                    CopyNDDynamic<T, VECLEN, ALIGNED, N-1>::template ConstSrc<OTHER_SRCDIMS...>::Accumulate(
                        src + i * SRC_STRIDE, dst + i * dst_stride, acc, otherdims...);
            }
            
            template <typename ACCUMULATE, typename... Args>
            static DACE_HDFI void Accumulate_atomic(const T *src, T *dst, ACCUMULATE acc, const int& copydim, const int& dst_stride, const Args&... otherdims)
            {
                __DACE_UNROLL
                for (int i = 0; i < copydim; ++i)
                    CopyNDDynamic<T, VECLEN, ALIGNED, N-1>::template ConstSrc<OTHER_SRCDIMS...>::Accumulate_atomic(
                        src + i * SRC_STRIDE, dst + i * dst_stride, acc, otherdims...);
            }
        };

        template <int DST_STRIDE, int... OTHER_DSTDIMS>
        struct ConstDst
        {
            template <typename... Args>
            static DACE_HDFI void Copy(const T *src, T *dst, const int& copydim, const int& src_stride, const Args&... otherdims)
            {
                __DACE_UNROLL
                for (int i = 0; i < copydim; ++i)
                    CopyNDDynamic<T, VECLEN, ALIGNED, N-1>::template ConstDst<OTHER_DSTDIMS...>::Copy(
                        src + i * src_stride, dst + i * DST_STRIDE, otherdims...);
            }

            template <typename ACCUMULATE, typename... Args>
            static DACE_HDFI void Accumulate(const T *src, T *dst, ACCUMULATE acc, const int& copydim, const int& src_stride, const Args&... otherdims)
            {
                __DACE_UNROLL
                for (int i = 0; i < copydim; ++i)
                    CopyNDDynamic<T, VECLEN, ALIGNED, N-1>::template ConstDst<OTHER_DSTDIMS...>::Accumulate(
                        src + i * src_stride, dst + i * DST_STRIDE, acc, otherdims...);
            }
            
            template <typename ACCUMULATE, typename... Args>
            static DACE_HDFI void Accumulate_atomic(const T *src, T *dst, ACCUMULATE acc, const int& copydim, const int& src_stride, const Args&... otherdims)
            {
                __DACE_UNROLL
                for (int i = 0; i < copydim; ++i)
                    CopyNDDynamic<T, VECLEN, ALIGNED, N-1>::template ConstDst<OTHER_DSTDIMS...>::Accumulate_atomic(
                        src + i * src_stride, dst + i * DST_STRIDE, acc, otherdims...);
            }
        };

        struct Dynamic
        {
            template <typename... Args>
            static DACE_HDFI void Copy(const T *src, T *dst, const int& copydim, const int& src_stride, const int& dst_stride, const Args&... otherdims)
            {
                static_assert(sizeof...(otherdims) == (N - 1) * 3, "Dimensionality mismatch in dynamic copy");
                __DACE_UNROLL
                for (int i = 0; i < copydim; ++i)
                    CopyNDDynamic<T, VECLEN, ALIGNED, N - 1>::Dynamic::Copy(
                        src + i * src_stride, dst + i * dst_stride, otherdims...);
            }

            template <typename ACCUMULATE, typename... Args>
            static DACE_HDFI void Accumulate(const T *src, T *dst, ACCUMULATE acc, const int& copydim, const int& src_stride, const int& dst_stride, const Args&... otherdims)
            {
                static_assert(sizeof...(otherdims) == (N - 1) * 3, "Dimensionality mismatch in dynamic copy");
                __DACE_UNROLL
                for (int i = 0; i < copydim; ++i)
                    CopyNDDynamic<T, VECLEN, ALIGNED, N - 1>::Dynamic::Accumulate(
                        src + i * src_stride, dst + i * dst_stride, acc, otherdims...);
            }
            
            template <typename ACCUMULATE, typename... Args>
            static DACE_HDFI void Accumulate_atomic(const T *src, T *dst, ACCUMULATE acc, const int& copydim, const int& src_stride, const int& dst_stride, const Args&... otherdims)
            {
                static_assert(sizeof...(otherdims) == (N - 1) * 3, "Dimensionality mismatch in dynamic copy");
                __DACE_UNROLL
                for (int i = 0; i < copydim; ++i)
                    CopyNDDynamic<T, VECLEN, ALIGNED, N - 1>::Dynamic::Accumulate_atomic(
                        src + i * src_stride, dst + i * dst_stride, acc, otherdims...);
            }
        };
    };

    template <typename T, int VECLEN, int ALIGNED>
    struct CopyNDDynamic<T, VECLEN, ALIGNED, 0>
    {
        template <int...>
        struct ConstSrc
        {
            static DACE_HDFI void Copy(const T *src, T *dst)
            {
                *(T *)dst = *(T *)src;
            }

            template <typename ACCUMULATE, typename... Args>
            static DACE_HDFI void Accumulate(const T *src, T *dst, ACCUMULATE acc)
            {
                *(T *)dst = acc(*(T *)dst, *(T *)src);
            }
            
            template <typename ACCUMULATE, typename... Args>
            static DACE_HDFI void Accumulate_atomic(const T *src, T *dst, ACCUMULATE acc)
            {
                wcr_custom<T>::reduce_atomic(acc, (T *)dst, *(T *)src);
            }
        };

        template <int...>
        struct ConstDst
        {
            static DACE_HDFI void Copy(const T *src, T *dst)
            {
                *(T *)dst = *(T *)src;
            }

            template <typename ACCUMULATE>
            static DACE_HDFI void Accumulate(const T *src, T *dst, ACCUMULATE acc)
            {
                *(T *)dst = acc(*(T *)dst, *(T *)src);
            }
                        
            template <typename ACCUMULATE>
            static DACE_HDFI void Accumulate_atomic(const T *src, T *dst, ACCUMULATE acc)
            {
                wcr_custom<T>::reduce_atomic(acc, (T *)dst, *(T *)src);
            }
        };

        struct Dynamic
        {
            static DACE_HDFI void Copy(const T *src, T *dst)
            {
                *(T *)dst = *(T *)src;
            }

            template <typename ACCUMULATE>
            static DACE_HDFI void Accumulate(const T *src, T *dst, ACCUMULATE acc)
            {
                *(T *)dst = acc(*(T *)dst, *(T *)src);
            }
                        
            template <typename ACCUMULATE>
            static DACE_HDFI void Accumulate_atomic(const T *src, T *dst, ACCUMULATE acc)
            {
                wcr_custom<T>::reduce_atomic(acc, (T *)dst, *(T *)src);
            }
        };
    };

}  // namespace dace



