import torch
from torch import nn
import torch.nn.functional as F
from daceml.torch import dace_module
from dace.codegen.targets.snitch import SnitchCodeGen
from pathlib import Path
from textwrap import dedent
import os
import dace
import subprocess
import argparse
from daceml.onnx.converters import clean_onnx_name
from itertools import chain
from simulation import run

B = 8
D1 = 784
D2 = 120
D3 = 32
D4 = 10

B = 2
D1 = 2
D2 = 2
D3 = 8
D4 = 2

@dace_module
class Net(nn.Module):
    def __init__(self):
        super().__init__()
        self.fc1 = nn.Linear(D1, D2)
        self.fc2 = nn.Linear(D2, D3)
        self.fc3 = nn.Linear(D3, D4)
        self.ls = nn.LogSoftmax(dim=-1)

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        x = self.ls(x)
        return x

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--simulator", type=str, default='banshee')
    parser.add_argument("--toolchain", type=str, default='llvm')
    args = parser.parse_args()
    
    x = torch.randn(B, D1)

    model = Net()
    
    run(model, [x], toolchain=args.toolchain, simulator=args.simulator, opts='-O3 -g')
