from dace import dtypes
import torch
from torch import nn
import torch.nn.functional as F
import daceml
from daceml.torch import dace_module
from dace.codegen.targets.snitch import SnitchCodeGen
from pathlib import Path
from textwrap import dedent
import os
import dace
import subprocess
import argparse
from daceml.onnx.converters import clean_onnx_name
from itertools import chain
from dace.transformation.auto.auto_optimize import set_fast_implementations
from math import prod
import itertools
import copy
import daceml.onnx as donnx
from toolchain import compile_and_run_snitch_code, compile_snitch_code
import re


def run_sdfg(sdfg, ref_sdfg, input_dict, toolchain='llvm', simulator='banshee', opts='-O3 -g'):    
    ref_input_dict = copy.deepcopy(input_dict)
    ref_sdfg(**ref_input_dict)
    
    set_fast_implementations(sdfg, dace.dtypes.DeviceType.CPU, blocklist='OpenMP')
    
    sdfg.expand_library_nodes()
    
    for node, parent in sdfg.all_nodes_recursive():
        if isinstance(node, dace.nodes.MapEntry):
            node.schedule = dace.ScheduleType.Sequential

    sdfg.save('layernorm_snitch.sdfg')

    code, header = SnitchCodeGen.gen_code_snitch(sdfg)

    generated_dir = Path('generated_kernel_snitch').resolve()
    
    dace_root = Path(f"{dace.__path__[0]}/..").resolve()
    fallback_snitch_root = dace_root / "../snitch"
    snitch_root = Path(os.environ.get('SNITCH_ROOT', fallback_snitch_root))
    if not snitch_root.is_dir():
        raise RuntimeError(f"SNITCH_ROOT is not found at {snitch_root}")
    
    fallback_snitch_toolchain = dace_root / f'../cmake/my-toolchain-{toolchain}-{simulator}.cmake'
    snitch_toolchain = Path(os.environ.get('SNITCH_CMAKE_TOOLCHAIN', fallback_snitch_toolchain))
    if not snitch_toolchain.is_file():
        raise RuntimeError(f"SNITCH_CMAKE_TOOLCHAIN is not found at {snitch_toolchain}")
    
    verilator_path = snitch_root / 'hw/system/snitch_cluster/bin/snitch_cluster.vlt'
    if not Path(verilator_path).is_file():
        raise RuntimeError(f"verilator is not found at {verilator_path}")
    
    banshee_path = snitch_root / 'sw/banshee/target/debug/banshee'
    if not Path(banshee_path).is_file():
        raise RuntimeError(f"banshee is not found at {banshee_path}")
    
    banshee_config = snitch_root / 'sw/banshee/config/snitch_cluster.yaml'
    if not Path(banshee_config).is_file():
        raise RuntimeError(f"banshee config is not found at {banshee_config}")
        
    generated_dir.mkdir(exist_ok=True)
    
    # adapted from dace/dace/codegen/codegen.py
    init_args = {
        k: dace.data.Scalar(sdfg.symbols[k]) 
        for k in sorted(sdfg.free_symbols)
        if not k.startswith('__dace')
    }
    program_args = sdfg.arglist()
        
    params_decl = []
    array_args = {
        name: data
        for name, data in program_args.items()
        if isinstance(data, dace.data.Array)
    }
    
    for name in program_args:
        data = program_args[name]
        value = input_dict[name]
        ref_value = ref_input_dict[name]
        if isinstance(data, dace.data.Array):
            params_decl.append(f"{data.dtype} {name}[{value.size}] = {{ {', '.join([f'{v}' for v in value.flatten()])} }};")
            params_decl.append(f"{data.dtype} ref_{name}[{ref_value.size}] = {{ {', '.join([f'{v}' for v in ref_value.flatten()])} }};")
        elif isinstance(data, dace.data.Scalar):
            params_decl.append(f"{data.dtype} {name} = {float(value)};")
        else:
            raise NotImplementedError()
    
    params_decl_str = '\n'.join(params_decl)
    params_init_str = ', '.join(init_args.keys())
    params_use_str = ', '.join(program_args.keys())
    param_verify_str = '\n'.join(
        f"""
        for (int i = 0; i < {input_dict[name].size}; i++) {{
            if (std::abs({name}[i] - ref_{name}[i]) >= 1e-3) {{
                printf(\"{name}[%d] expected %f but got %f\\n\", i, ref_{name}[i], {name}[i]);
                return 1;
            }}
        }}
        """ for name in array_args
    )
    
    main_caller = dedent(f"""
        #include "kernel.h"
        #include "snrt.h"
        #include "omp.h"
        #include "dm.h"
        #include <cstdio>
        #include <cmath>
        
        {params_decl_str}
        
        int main() {{
            unsigned core_idx = snrt_cluster_core_idx();
            unsigned core_num = snrt_cluster_core_num();

            if (core_idx != 0) return 0;

            {sdfg.name}Handle_t handle = __dace_init_{sdfg.name}({params_init_str});

            unsigned long t1 = read_csr(mcycle);
            __program_{sdfg.name}(handle, {params_use_str});
            unsigned long t2 = read_csr(mcycle);
            printf("Cycles: %lu\\n", t2 - t1);
            
            {param_verify_str}
            
            __dace_exit_{sdfg.name}(handle);
                                        
            return 0;
        }}
    """)
    
    code1 = ""
    for line in code.split('\n'):
        if "#pragma omp" not in line:
            code1 += line + '\n'

    with open(generated_dir / "kernel.cpp", "w") as fd:
        fd.write(code1)
    with open(generated_dir / "kernel.h", "w") as fd:
        fd.write(header)
    with open(generated_dir / "kernel_caller.cpp", "w") as fd:
        fd.write(main_caller)
    with open(generated_dir / "CMakeLists.txt", "w") as fd:
        fd.write(dedent(f"""
            cmake_minimum_required(VERSION 3.13)
            project(kernel_caller LANGUAGES C CXX ASM)
            add_executable(kernel_caller kernel_caller.cpp kernel.cpp)
            
            add_custom_command(
                TARGET kernel_caller
                POST_BUILD
                COMMAND ${{CMAKE_OBJDUMP}} -dhS $<TARGET_FILE:kernel_caller> > $<TARGET_FILE:kernel_caller>.s)
            
            target_include_directories(kernel_caller PUBLIC {dace_root}/dace/runtime/include/)
            target_compile_definitions(kernel_caller PUBLIC __SNITCH__)
            target_compile_options(kernel_caller PUBLIC {opts})
        """))
        
    build_dir = generated_dir / 'build'
    test_file = build_dir / 'kernel_caller'
    cmake_cache = build_dir / 'CMakeCache.txt'
    
    if Path(cmake_cache).is_file():
        Path(cmake_cache).unlink()
        
    Path(build_dir).mkdir(exist_ok=True)
    
    if simulator == 'banshee':
        subprocess.check_call([
            'cmake',
            '..',
            f'-DCMAKE_TOOLCHAIN_FILE={snitch_toolchain}',
        ], cwd=build_dir)
        
        subprocess.check_call(['cmake', '--build', build_dir, '--verbose'])
        
        subprocess.check_call([
            banshee_path,
            '--configuration',
            banshee_config,
            '--latency',
            test_file
        ], cwd=build_dir)
        
    elif simulator == 'verilator':
        subprocess.check_call([
            'cmake',
            '..',
            f'-DCMAKE_TOOLCHAIN_FILE={snitch_toolchain}',
        ], cwd=build_dir)
        
        subprocess.check_call(['cmake', '--build', build_dir, '--verbose'])
        
        subprocess.check_call([
            verilator_path,
            test_file
        ], cwd=build_dir)
    else:
        raise RuntimeError('Unknown simulator')


def run(model, inputs, toolchain='llvm', simulator='banshee', verify=True, opts='-O3 -g'):
    predictions = model(*inputs)
    if not isinstance(predictions, (list, tuple)):
        predictions = [predictions]

    donnx.default_implementation = "pure"

    sdfg = model.sdfg
    assert sdfg is not None
    
    set_fast_implementations(sdfg, dace.dtypes.DeviceType.CPU, blocklist='OpenMP')
    
    sdfg.expand_library_nodes()
    
    for node, parent in sdfg.all_nodes_recursive():
        if isinstance(node, dace.nodes.MapEntry):
            node.schedule = dace.ScheduleType.Sequential
    
    for _, _, array in sdfg.arrays_recursive():
        if array.storage in (dace.dtypes.StorageType.Default, dace.dtypes.StorageType.Register) and prod(array.shape) > 10:
            array.storage = dace.dtypes.StorageType.CPU_Heap

    code, header = SnitchCodeGen.gen_code_snitch(sdfg)
    code = code.replace('#include "dace/dace.h"', '#include "dace_mini.h"')
    code = code.replace('#include <omp.h>', '')

    # code = re.sub(r'struct\s+([^\s{]+)\s*{([^}]*)};', r'typedef struct {\2} \1;', code, re.M)
    # code = re.sub(r'dace::wcr_fixed<dace::ReductionType::Sum, float>::reduce\(([^,]+),([^)]+)\)', r'*(\1) += \2', code)
    # code = re.sub(r'dace::wcr_fixed<dace::ReductionType::Max, float>::reduce\(([^,]+),([^)]+)\)', r'*(\1) = *(\1) > (\2) ? *(\1) : (\2)', code)
    code = re.sub(r'DACE_ALIGN\([^)]+\)', r'', code)
    # code = code.replace('dace::float32', '(float)')
    # code = code.replace('max', 'fmax')
    # code = code.replace('dace::math::pow', 'pow')
    code = re.sub(r'= new ([^[]+)\[([^\]]+)\];', r'= (\1*) snrt_l3alloc(sizeof(\1) * (\2));', code)
    code = re.sub(r'= new ([^;]+);', r'= (\1*) snrt_l3alloc(sizeof(\1));', code)
    code = re.sub(r'delete\[\]([^;]+);', r'// snrt_l3free(\1);', code)
    code = re.sub(r'delete([^;]+);', r'// snrt_l3free(\1);', code)
    # code = code.replace('DACE_EXPORTED', 'extern "C"')
    # code = code.replace('nullptr', 'NULL')

    # dace::CopyND<dtype, veclen, aligned, *copy_size>::template ConstDst<*dst_stride>::Copy(src, dst, *src_stride);
    # 3D case: dace::CopyND<dtype, veclen, aligned, n1, n2, n3>::template ConstDst<d1, d2, d3>::Copy(src, dst, s1, s2, s3);
    # code = re.sub(
    #     r'dace::CopyND<([^,]+), ([^,]+), ([^,]+), ([^,]+), ([^,]+), ([^>]+)>::template ConstDst<([^,]+), ([^,]+), ([^>]+)>::Copy\(([^,]+), ([^,]+), ([^,]+), ([^,]+), ([^)]+)\);',
    #     r'''
    #         for (int cpy0 = 0; cpy0 < (\4); cpy0++)
    #         for (int cpy1 = 0; cpy1 < (\5); cpy1++)
    #         for (int cpy2 = 0; cpy2 < (\6); cpy2++)
    #             (\11)[cpy0 * (\7) + cpy1 * (\8) + cpy2 * (\9)] = (\10)[cpy0 * (\12) + cpy1 * (\13) + cpy2 * (\14)];
    #     ''',
    #     code,
    # )
    # # 1D case
    # code = re.sub(
    #     r'dace::CopyND<([^,]+), ([^,]+), ([^,]+), ([^>]+)>::template ConstDst<([^>]+)>::Copy\(([^,]+), ([^,]+), ([^)]+)\);',
    #     r'''
    #         for (int cpy0 = 0; cpy0 < (\4); cpy0++)
    #             (\7)[cpy0 * (\5)] = (\6)[cpy0 * (\8)];
    #     ''',
    #     code,
    # )
    
    # divisions
    # code = re.sub(r'\((\w+) / (\w+)\)', r'my_div(\1, \2)', code)

    generated_dir = Path('generated_kernel_snitch').resolve()
    
    # dace_root = Path(f"{dace.__path__[0]}/..").resolve()
    fallback_snitch_root = Path("/repo/snitch").resolve()
    # fallback_snitch_root = dace_root / "../snitch"
    snitch_root = Path(os.environ.get('SNITCH_ROOT', fallback_snitch_root))
    if not snitch_root.is_dir():
        raise RuntimeError(f"SNITCH_ROOT is not found at {snitch_root}")

    # verilator_path = snitch_root / 'hw/system/snitch_cluster/bin/snitch_cluster.vlt'
    verilator_path = Path("/repo/snitch_cluster.vlt").resolve()
    if simulator == "verilator" and not Path(verilator_path).is_file():
        raise RuntimeError(f"verilator is not found at {verilator_path}")
    
    # banshee_path = snitch_root / 'sw/banshee/target/debug/banshee'
    banshee_path = Path("/tools/bin/banshee").resolve()
    if not Path(banshee_path).is_file():
        raise RuntimeError(f"banshee is not found at {banshee_path}")
    
    banshee_config = Path('/repo/snitch_cluster.yaml').resolve()
    if not Path(banshee_config).is_file():
        raise RuntimeError(f"banshee config is not found at {banshee_config}")
        
    generated_dir.mkdir(exist_ok=True)
    
    c_dtype = {
        torch.float32: 'float',
        torch.int64: 'int64_t',
        torch.int32: 'int32_t',
    }


    if isinstance(model, daceml.onnx.ONNXModel):
        # onnx version (run_onnx)
        
        clean_weights = model.clean_weights
    
        sdfg_inputs, symbols, sdfg_outputs = model._call_args(args=inputs, kwargs={})

        sdfg_params = model.initialized_parameters
        all_params_no_order = {**sdfg_inputs, **sdfg_outputs, **sdfg_params, **clean_weights}
        all_params = {n: all_params_no_order[n] for n in sdfg.arglist()}
        
        out_names = list(sdfg_outputs.keys())
        sdfg_ref_outputs = {n: p for n, p in zip(out_names, predictions)}
    
    elif isinstance(model, daceml.torch.DaceModule):
        # pytorch model version (run)
                
        sdfg_params = {clean_onnx_name(k): v for k, v in model._exported_parameters.items()}
        sdfg_inputs_list = [clean_onnx_name(n) for n in model.dace_model.inputs[:len(inputs)]]
        sdfg_inputs = {x: y for x, y in zip(sdfg_inputs_list, inputs)}

        sdfg_outputs_list = model.dace_model.outputs
        assert len(sdfg_outputs_list) == 1
        prediction = predictions[0]
        sdfg_outputs = {sdfg_outputs_list[0]: torch.zeros_like(prediction)}
        
        clean_weights = model.dace_model.clean_weights
        all_params_no_order = {**sdfg_inputs, **sdfg_outputs, **sdfg_params, **clean_weights}
        all_params = {n: all_params_no_order[n] for n in sdfg.arglist()}
        
        out_names = list(sdfg_outputs.keys())

        sdfg_ref_outputs = {sdfg_outputs_list[0]: prediction}
        out_name = sdfg_outputs_list[0]
    else:
        raise ValueError(f"Unknown model type: {type(model)}")
    
        
    params_init = []
    params_use = []

    for n, p in all_params.items():
        if p.shape:  # general case
            if verify or p.numel() < 20:
                params_init.append(f"{c_dtype[p.dtype]} {n}[{p.numel()}] = {{ {', '.join([f'{v}' for v in p.flatten()])} }};")
            else:
                params_init.append(f"{c_dtype[p.dtype]} {n}[{p.numel()}];")
        else:  # scalar
            params_init.append(f"{c_dtype[p.dtype]} {n} = {p};")
        params_use.append(n)
    for n, p in sdfg_ref_outputs.items():
        if verify or p.numel() < 20:
            params_init.append(f"{c_dtype[p.dtype]} ref_{n}[{p.numel()}] = {{ {', '.join([f'{v}' for v in p.flatten()])} }};")
        else:
            params_init.append(f"{c_dtype[p.dtype]} ref_{n}[{p.numel()}];")

    
    params_init_str = '\n'.join(params_init)
    params_use_str = ', '.join(params_use)

    main_caller = dedent(f"""
        #include <math.h>
        #include "kernel.h"
        #include "snrt.h"
        #include "printf.h"
                         
        {params_init_str}
        
        int main() {{
            unsigned core_idx = snrt_cluster_core_idx();
            if (core_idx != 0) return 0;

            int err = 0;
            
            {sdfg.name}Handle_t handle = __dace_init_{sdfg.name}();

            unsigned long t1 = read_csr(mcycle);
            __program_{sdfg.name}(handle, {params_use_str});
            unsigned long t2 = read_csr(mcycle);
            printf("Cycles: %lu\\n", t2 - t1);
    """)

    if verify:
        for out_name, prediction in zip(out_names, predictions):
            main_caller += dedent(f"""
                for (int i = 0; i < {prediction.numel()}; i++) {{
                    if (fabs({out_name}[i] - ref_{out_name}[i]) >= 1e-3) {{
                        printf("Mismatch: %f %f\\n", {out_name}[i], ref_{out_name}[i]);
                        err = 1;
                        break;
                    }}
                }}
            """)
    
    main_caller += dedent(f"""
            __dace_exit_{sdfg.name}(handle);

            printf("Done, err %d\\n", (int)err);
                                        
            return err;
        }}
    """)

    main_src = generated_dir / "kernel_caller.cpp"
    code_src = generated_dir / "kernel.cpp"
    bin_src = generated_dir / "kernel_bin"
    
    with open(generated_dir / "kernel.cpp", "w") as fd:
        fd.write(code)
    with open(generated_dir / "kernel.h", "w") as fd:
        fd.write(header)
    with open(generated_dir / "kernel_caller.cpp", "w") as fd:
        fd.write(main_caller)
    
    sim = {
        'banshee': 'banshee',
        'verilator': 'rtl',
    }[simulator]
    cycles = compile_and_run_snitch_code([code_src, main_src], bin_src, simulator=sim)
    print("Cycles (parsed):", cycles)

    # if simulator == 'banshee':
    #     subprocess.check_call([
    #         'cmake',
    #         '..',
    #         f'-DCMAKE_TOOLCHAIN_FILE={snitch_toolchain}',
    #     ], cwd=build_dir)
        
    #     subprocess.check_call(['cmake', '--build', build_dir, '--verbose'])
        
    #     subprocess.check_call([
    #         banshee_path,
    #         #'--latency',
    #         '--configuration',
    #         banshee_config,
    #         test_file
    #     ], cwd=build_dir)
        
    # elif simulator == 'verilator':
    #     subprocess.check_call([
    #         'cmake',
    #         '..',
    #         f'-DCMAKE_TOOLCHAIN_FILE={snitch_toolchain}',
    #     ], cwd=build_dir)
        
    #     subprocess.check_call(['cmake', '--build', build_dir, '--verbose'])
        
    #     subprocess.check_call([
    #         verilator_path,
    #         test_file
    #     ], cwd=build_dir)
    # else:
    #     raise RuntimeError('Unknown simulator')
    
    
def run_onnx(model, inputs, toolchain='llvm', simulator='banshee', opts='-O3 -g'):
    predictions = model(*inputs)
    if not isinstance(predictions, (list, tuple)):
        predictions = [predictions]

    donnx.default_implementation = "pure"

    sdfg = model.sdfg
    assert sdfg is not None
    
    set_fast_implementations(sdfg, dace.dtypes.DeviceType.CPU, blocklist='OpenMP')
    
    sdfg.expand_library_nodes()
    
    for node, parent in sdfg.all_nodes_recursive():
        if isinstance(node, dace.nodes.MapEntry):
            node.schedule = dace.ScheduleType.Sequential

    code, header = SnitchCodeGen.gen_code_snitch(sdfg)

    generated_dir = Path('generated_kernel_snitch').resolve()
    
    dace_root = Path(f"{dace.__path__[0]}/..").resolve()
    fallback_snitch_root = dace_root / "../snitch"
    snitch_root = Path(os.environ.get('SNITCH_ROOT', fallback_snitch_root))
    if not snitch_root.is_dir():
        raise RuntimeError(f"SNITCH_ROOT is not found at {snitch_root}")
    
    fallback_snitch_toolchain = dace_root / f'../cmake/my-toolchain-{toolchain}-{simulator}.cmake'
    snitch_toolchain = Path(os.environ.get('SNITCH_CMAKE_TOOLCHAIN', fallback_snitch_toolchain))
    if not snitch_toolchain.is_file():
        raise RuntimeError(f"SNITCH_CMAKE_TOOLCHAIN is not found at {snitch_toolchain}")
    
    verilator_path = snitch_root / 'hw/system/snitch_cluster/bin/snitch_cluster.vlt'
    if not Path(verilator_path).is_file():
        raise RuntimeError(f"verilator is not found at {verilator_path}")
    
    banshee_path = snitch_root / 'sw/banshee/target/debug/banshee'
    if not Path(banshee_path).is_file():
        raise RuntimeError(f"banshee is not found at {banshee_path}")
    
    banshee_config = snitch_root / 'sw/banshee/config/snitch_cluster.yaml'
    if not Path(banshee_config).is_file():
        raise RuntimeError(f"banshee config is not found at {banshee_config}")
        
    generated_dir.mkdir(exist_ok=True)
        
    sdfg_params = model.initialized_parameters
        
    sdfg_inputs, symbols, sdfg_outputs = model._call_args(args=inputs, kwargs={})
    
    # sdfg outputs are not initialized, 
    # let's initialize them with zeros to make it easier to debug
    for v in sdfg_outputs.values():
        v.zero_()
        
    clean_weights = model.clean_weights
    
    all_params_no_order = {**sdfg_inputs, **sdfg_outputs, **sdfg_params, **clean_weights}
    all_params = {n: all_params_no_order[n] for n in sdfg.arglist()}
    
    out_names = list(sdfg_outputs.keys())

    sdfg_ref_outputs = {n: p for n, p in zip(out_names, predictions)}
        
    params_init = []
    params_use = []
    
    c_dtype = {
        torch.float32: 'float',
        torch.int64: 'long long',
    }
    
    for n, p in all_params.items():
        if p.shape:  # general case
            params_init.append(f"{c_dtype[p.dtype]} {n}[{p.numel()}] = {{ {', '.join([f'{v}' for v in p.flatten()])} }};")
        else:  # scalar
            params_init.append(f"{c_dtype[p.dtype]} {n} = {p};")
        params_use.append(n)
    for n, p in sdfg_ref_outputs.items():
        params_init.append(f"{c_dtype[p.dtype]} ref_{n}[{p.numel()}] = {{ {', '.join([f'{v}' for v in p.flatten()])} }};")
    
    params_init_str = '\n'.join(params_init)
    params_use_str = ', '.join(params_use)
    

    main_caller = dedent(f"""
        #include "kernel.h"
        #include "snrt.h"
        #include "omp.h"
        #include "dm.h"
        #include <cstdio>
        #include <cmath>
        
        {params_init_str}
        
        int main() {{
            unsigned core_idx = snrt_cluster_core_idx();
            unsigned core_num = snrt_cluster_core_num();
            
            int err = 0;
            
            __snrt_omp_bootstrap(core_idx);

            {sdfg.name}Handle_t handle = __dace_init_{sdfg.name}();

            __program_{sdfg.name}(handle, {params_use_str});
    """)

    for out_name, prediction in zip(out_names, predictions):
        main_caller += dedent(f"""
            for (int i = 0; i < {prediction.numel()}; i++) {{
                if (std::abs({out_name}[i] - ref_{out_name}[i]) >= 1e-3) {{
                    printf("Mismatch: %f %f\\n", {out_name}[i], ref_{out_name}[i]);
                    err = 1;
                    break;
                }}
            }}
        """)
    
    main_caller += dedent(f"""
            __dace_exit_{sdfg.name}(handle);

            printf("Done, err %d\\n", (int)err);
            
            __snrt_omp_destroy(core_idx);
                            
            return err;
        }}
    """)
    
    with open(generated_dir / "kernel.cpp", "w") as fd:
        fd.write(code)
    with open(generated_dir / "kernel.h", "w") as fd:
        fd.write(header)
    with open(generated_dir / "kernel_caller.cpp", "w") as fd:
        fd.write(main_caller)
    with open(generated_dir / "CMakeLists.txt", "w") as fd:
        fd.write(dedent(f"""
            cmake_minimum_required(VERSION 3.13)
            project(kernel_caller LANGUAGES C CXX ASM)
            #list(APPEND CMAKE_MODULE_PATH {snitch_root}/sw/cmake)
            #include(SnitchUtilities)
            #add_subdirectory({snitch_root}/sw/snRuntime snRuntime)
            #add_snitch_executable(kernel_caller kernel_caller.cpp kernel.cpp)
            add_executable(kernel_caller kernel_caller.cpp kernel.cpp)
            
            add_custom_command(
                TARGET kernel_caller
                POST_BUILD
                COMMAND ${{CMAKE_OBJDUMP}} -dhS $<TARGET_FILE:kernel_caller> > $<TARGET_FILE:kernel_caller>.s)
            
            target_include_directories(kernel_caller PUBLIC {dace_root}/dace/runtime/include/)
            target_compile_definitions(kernel_caller PUBLIC __SNITCH__)
            target_compile_options(kernel_caller PUBLIC {opts})
        """))
        
    build_dir = generated_dir / 'build'
    test_file = build_dir / 'kernel_caller'
    cmake_cache = build_dir / 'CMakeCache.txt'
    
    if Path(cmake_cache).is_file():
        Path(cmake_cache).unlink()
        
    Path(build_dir).mkdir(exist_ok=True)
    
    if simulator == 'banshee':
        subprocess.check_call([
            'cmake',
            '..',
            f'-DCMAKE_TOOLCHAIN_FILE={snitch_toolchain}',
        ], cwd=build_dir)
        
        subprocess.check_call(['cmake', '--build', build_dir, '--verbose'])
        
        subprocess.check_call([
            banshee_path,
            '--configuration',
            banshee_config,
            test_file
        ], cwd=build_dir)
        
    elif simulator == 'verilator':
        subprocess.check_call([
            'cmake',
            '..',
            f'-DCMAKE_TOOLCHAIN_FILE={snitch_toolchain}',
        ], cwd=build_dir)
        
        subprocess.check_call(['cmake', '--build', build_dir, '--verbose'])
        
        subprocess.check_call([
            verilator_path,
            test_file
        ], cwd=build_dir)
    else:
        raise RuntimeError('Unknown simulator')