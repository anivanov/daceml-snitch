export SNITCH_COMPILER="${CURDIR}/toolchain/bin/clang++"
export SNITCH_DEFINITIONS="-D__LINK_LLD -D__TOOLCHAIN_LLVM__ -D__SNITCH__"
export SNITCH_INCLUDES="-I${CURDIR}/snitch/sw/snRuntime/include -I${CURDIR}/snitch/sw/snRuntime/vendor -I${CURDIR}/snitch/sw/snRuntime/vendor/riscv-opcodes -isystem ${HOME}/code/daceml-snitch/riscv64-unknown-elf-gcc/riscv64-unknown-elf/include/c++/8.3.0/riscv64-unknown-elf/rv32imfd/ilp32d/ -isystem ${HOME}/code/daceml-snitch/riscv64-unknown-elf-gcc/riscv64-unknown-elf/include/c++/8.3.0/ -isystem ${HOME}/code/daceml-snitch/riscv64-unknown-elf-gcc/riscv64-unknown-elf/8.3.0/include/ -isystem ${HOME}/code/daceml-snitch/riscv64-unknown-elf-gcc/riscv64-unknown-elf/include/"
export SNITCH_COMPILE_OPTIONS="-flto=thin -mcpu=snitch -mcmodel=medany -ffast-math -fno-builtin-printf -fno-common -ffunction-sections -Wextra -static -mllvm -enable-misched=false -mno-relax -fopenmp -menable-experimental-extensions"
export SNITCH_LINK_OPTIONS="-flto=thin -mcpu=snitch -nostartfiles -fuse-ld=lld -Wl,--image-base=0x80000000 -static -Wl,-z,norelro -Wl,--gc-sections -Wl,--no-relax -nostdlib -nodefaultlibs -nostdlib++"
export SNITCH_LINK_SCRIPT="${CURDIR}/snitch/sw/snRuntime/build/common.ld"

export SNITCH_LIBS="-lm ${CURDIR}/snitch/sw/snRuntime/build/libsnRuntime-cluster.a"
export SNITCH_LIBS="${SNITCH_LIBS} ${HOME}/code/daceml-snitch/riscv64-unknown-elf-gcc/riscv64-unknown-elf/lib/rv32imfd/ilp32d/libc.a"
export SNITCH_LIBS="${SNITCH_LIBS} ${HOME}/code/daceml-snitch/riscv64-unknown-elf-gcc/riscv64-unknown-elf/lib/rv32imfd/ilp32d/libm.a"
export SNITCH_LIBS="${SNITCH_LIBS} ${HOME}/code/daceml-snitch/riscv64-unknown-elf-gcc/riscv64-unknown-elf/lib/rv32imfd/ilp32d/libsupc++.a"
export SNITCH_LIBS="${SNITCH_LIBS} ${HOME}/code/daceml-snitch/riscv64-unknown-elf-gcc/riscv64-unknown-elf/lib/rv32imfd/ilp32d/libnosys.a"

export SNITCH_LIBS="${SNITCH_LIBS} ${HOME}/code/daceml-snitch/riscv64-unknown-elf-gcc/lib/gcc/riscv64-unknown-elf/8.3.0/rv32imfd/ilp32d/libgcc.a"

# riscv64-unknown-elf-gcc/riscv64-unknown-elf/lib/rv32imfd/ilp32d/libnosys.a
# riscv64-unknown-elf-gcc/riscv64-unknown-elf/lib/rv32imfd/ilp32d/libsim.a
# riscv64-unknown-elf-gcc/riscv64-unknown-elf/lib/rv32imfd/ilp32d/libstdc++.a
# riscv64-unknown-elf-gcc/riscv64-unknown-elf/lib/rv32imfd/ilp32d/libm.a
# riscv64-unknown-elf-gcc/riscv64-unknown-elf/lib/rv32imfd/ilp32d/libc_nano.a
# riscv64-unknown-elf-gcc/riscv64-unknown-elf/lib/rv32imfd/ilp32d/libg_nano.a
# riscv64-unknown-elf-gcc/riscv64-unknown-elf/lib/rv32imfd/ilp32d/libgloss.a
# riscv64-unknown-elf-gcc/riscv64-unknown-elf/lib/rv32imfd/ilp32d/libg.a
# riscv64-unknown-elf-gcc/riscv64-unknown-elf/lib/rv32imfd/ilp32d/libc.a
# riscv64-unknown-elf-gcc/riscv64-unknown-elf/lib/rv32imfd/ilp32d/libsupc++.a
# riscv64-unknown-elf-gcc/riscv64-unknown-elf/lib/rv32imfd/ilp32d/libgloss_nano.a