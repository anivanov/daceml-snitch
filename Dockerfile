FROM ghcr.io/pulp-platform/snitch_cluster:main@sha256:7c80c129d29b15778d09a5a8c7e74b3a6b1f7b2f6e59a8d96f4cf11a6fb41447
#FROM ghcr.io/pulp-platform/snitch_cluster:main
WORKDIR /tools
RUN pip install torch"<="1.13.1 torchvision"<="0.14.1 --index-url https://download.pytorch.org/whl/cpu
RUN git clone --recurse-submodules --shallow-submodules --depth=1 --branch snitch_codegen https://github.com/and-ivanov/dace.git dace
RUN cd dace && pip install -e .
RUN git clone --recurse-submodules --shallow-submodules --depth=1 --branch snitch_support https://github.com/and-ivanov/daceml.git daceml
RUN sed -i -e 's;onnx==1.7.0;onnx==1.9.0;g' /tools/daceml/setup.py  # ModuleNotFoundError: No module named 'cmake'
RUN cd daceml && pip install -e .
RUN pip install numpy"<"1.24.0 onnxruntime
RUN wget https://static.dev.sifive.com/dev-tools/riscv64-unknown-elf-gcc-8.3.0-2020.04.0-x86_64-linux-ubuntu14.tar.gz && \
    mkdir -p riscv && tar -x -f riscv64-unknown-elf-gcc-8.3.0-2020.04.0-x86_64-linux-ubuntu14.tar.gz --strip-components=1 -C riscv && \
    rm -rf riscv64-unknown-elf-gcc-8.3.0-2020.04.0-x86_64-linux-ubuntu14.tar.gz  # https://github.com/pulp-platform/snitch_cluster/commit/9751e6515ca4818d59e245b18beb4a4aa24da413#diff-146688a4310f12e16a776e77ffe46b884bfed32cdb688cdb3895c08e802efe35L92
RUN pip install opencv-python psutil tqdm seaborn scipy ultralytics
RUN apt-get update && apt-get install ffmpeg libsm6 libxext6 liblzma-dev -y
RUN pip install backports.lzma && sed -i -e 's;_lzma;backports.lzma;g' /opt/python/lib/python3.9/lzma.py  # ModuleNotFoundError: No module named '_lzma' https://github.com/ultralytics/yolov5/issues/1298
RUN cd /tools/riscv/bin && for f in riscv64*; do ln -s $f $(echo $f | sed -e 's/riscv64/riscv32/'); done
ENV PATH=/tools/riscv/bin:$PATH
RUN pip install transformers datasets scikit-learn
# RUN git clone --recurse-submodules --shallow-submodules --depth=1 --branch dace_support https://github.com/and-ivanov/snitch.git snitch
# RUN cd snitch/hw/system/snitch_cluster && make bin/snitch_cluster.vlt
# RUN apt-get update && apt-get install -y protobuf-compiler
# RUN wget https://github.com/orausch/onnxruntime/releases/download/v2/onnxruntime-daceml-patched.tgz
# RUN tar -xzvf onnxruntime-daceml-patched.tgz
# ENV ORT_RELEASE=/tools/onnxruntime-daceml-patched
# RUN pip install torch"<="1.13.1 torchvision"<="0.14.1
# RUN pip install onnxruntime scipy tensorflow
# ENV SNITCH_CMAKE_TOOLCHAIN=/tools/toolchain-llvm-banshee.cmake
# ENV SNITCH_ROOT=/tools/snitch
# RUN git clone --depth=1 https://github.com/spcl/npbench.git
# RUN pip install -r npbench/requirements.txt
# RUN pip install -e npbench
WORKDIR /workspace
