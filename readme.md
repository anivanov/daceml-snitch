Getting the code of this repository

```
git clone --recurse-submodules --shallow-submodules https://spclgitlab.ethz.ch/anivanov/daceml-snitch.git daceml-snitch
git clone --recurse-submodules --shallow-submodules git@spclgitlab.ethz.ch:anivanov/daceml-snitch.git daceml-snitch
```

Tested small targets (compilation + simulation)

```
make run-mlp_simple
make run-mlp_simple-vlt
make run-transformer
make run-transformer-vlt
```

Tested only compilation (workload is too big for simulation)

```
make run-yolov5
make run-yolov5-vlt
make run-yolov5-onnx
make run-yolov5-onnx-vlt
```