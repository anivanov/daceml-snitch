
float __divsf3(float a, float b) {
    double c;
    asm ("fdiv.s %0, %1, %2" : "=f"(c) : "f"(a), "f"(b));
    return c;
}

double __divdf3(double a, double b) {
    double c;
    asm ("fdiv.d %0, %1, %2" : "=f"(c) : "f"(a), "f"(b));
    return c;
}