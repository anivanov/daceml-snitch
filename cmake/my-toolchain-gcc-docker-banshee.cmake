include(${CMAKE_CURRENT_LIST_DIR}/toolchain-gcc.cmake)


add_link_options(-T "/tools/snRuntime-gcc-build/common.ld")

foreach(LANG  C CXX)
    set(CMAKE_${LANG}_STANDARD_LIBRARIES "${CMAKE_${LANG}_STANDARD_LIBRARIES} /tools/snRuntime-gcc-build/libsnRuntime-banshee.a"
    )
    list(APPEND CMAKE_${LANG}_STANDARD_INCLUDE_DIRECTORIES 
        "/tools/snitch/sw/snRuntime/include/"
        "/tools/snitch/sw/vendor/riscv-opcodes/"
    )
endforeach()
