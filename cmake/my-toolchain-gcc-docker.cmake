# adapted from snitch/sw/cmake/toolchain-gcc.cmake

# Copyright 2020 ETH Zurich and University of Bologna.
# Solderpad Hardware License, Version 0.51, see LICENSE for details.
# SPDX-License-Identifier: SHL-0.51

set(TOOLCHAIN "/tools/riscv-gnu-install")
set(CMAKE_C_COMPILER "${TOOLCHAIN}/bin/riscv32-unknown-elf-gcc")
set(CMAKE_CXX_COMPILER "${TOOLCHAIN}/bin/riscv32-unknown-elf-g++")
set(CMAKE_OBJCOPY "${TOOLCHAIN}/bin/riscv32-unknown-elf-objcopy")
set(CMAKE_OBJDUMP "${TOOLCHAIN}/bin/riscv32-unknown-elf-objdump")
set(CMAKE_AR "${TOOLCHAIN}/bin/riscv32-unknown-elf-gcc-ar")
set(CMAKE_RANLIB "${TOOLCHAIN}/bin/riscv32-unknown-elf-gcc-ranlib")

# LTO
set(CMAKE_INTERPROCEDURAL_OPTIMIZATION true)
set(CMAKE_C_COMPILER_AR "${CMAKE_AR}")
set(CMAKE_CXX_COMPILER_AR "${CMAKE_AR}")
set(CMAKE_C_COMPILER_RANLIB "${CMAKE_RANLIB}")
set(CMAKE_CXX_COMPILER_RANLIB "${CMAKE_RANLIB}")


add_compile_options(-march=rv32imafd -mabi=ilp32d -mcmodel=medany -mno-fdiv -ffast-math -fno-builtin-printf -fno-common)
#add_compile_options(-march=rv32imafd -mabi=ilp32d -mcmodel=medany -mno-fdiv -ffast-math -fno-common)
add_link_options(-march=rv32imafd -mabi=ilp32d -nostartfiles -Wl,-Ttext-segment=0x80000000 )
#add_link_options(-Wl,--verbose)
add_link_options(-nodefaultlibs)
#add_link_options(-specs=nosys.specs)

add_compile_options(-nostdinc)

#add_compile_options(-ffunction-sections)
add_compile_options(-Wextra)

# Add preprocessor definition to indicate LD is used
add_compile_definitions(__LINK_LD)
add_compile_definitions(__TOOLCHAIN_GCC__)



foreach(LANG  C CXX)
    set(CMAKE_${LANG}_STANDARD_LIBRARIES 
        # C++ stdlib
        "/tools/riscv-gnu-install/riscv32-unknown-elf//lib/libstdc++.a"
        # C++ runtime
        "/tools/riscv-gnu-install/lib/gcc/riscv32-unknown-elf/12.2.0/crtbegin.o"
        # C stdlib
        "/tools/newlib-install/riscv32-unknown-elf/lib/libc.a"
        "/tools/newlib-install/riscv32-unknown-elf/lib/libm.a"
        # GCC runtime
        "/tools/riscv-gnu-install/lib/gcc/riscv32-unknown-elf/12.2.0/libgcc.a"
    )
    # convert ';' separated string to ' ' separated string
    list(JOIN CMAKE_${LANG}_STANDARD_LIBRARIES " " CMAKE_${LANG}_STANDARD_LIBRARIES)
    set(CMAKE_${LANG}_STANDARD_INCLUDE_DIRECTORIES 
        # C++ stdlib
        "/tools/riscv-gnu-install/riscv32-unknown-elf/include/c++/12.2.0/riscv32-unknown-elf/"
        "/tools/riscv-gnu-install/riscv32-unknown-elf/include/c++/12.2.0/"
        # C stdlib
        "/tools/newlib-install/riscv32-unknown-elf/include/"
        # GCC runtime
        "/tools/riscv-gnu-install/lib/gcc/riscv32-unknown-elf/12.2.0/include"
    )
endforeach()