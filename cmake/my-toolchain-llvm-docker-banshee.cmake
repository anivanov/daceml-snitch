include(${CMAKE_CURRENT_LIST_DIR}/my-toolchain-llvm-docker.cmake)


add_link_options(-T "/repo/snRuntime-build/common.ld")

foreach(LANG  C CXX)
    set(CMAKE_${LANG}_STANDARD_LIBRARIES "${CMAKE_${LANG}_STANDARD_LIBRARIES} /repo/snRuntime-build/libsnRuntime-banshee.a"
    )
    list(APPEND CMAKE_${LANG}_STANDARD_INCLUDE_DIRECTORIES 
        "/repo/snitch/sw/snRuntime/include/"
        "/repo/snitch/sw/vendor/riscv-opcodes/"
    )
endforeach()
