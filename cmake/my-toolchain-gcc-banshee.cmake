# adapted from snitch/sw/cmake/toolchain-gcc.cmake

# Copyright 2020 ETH Zurich and University of Bologna.
# Solderpad Hardware License, Version 0.51, see LICENSE for details.
# SPDX-License-Identifier: SHL-0.51

include(${CMAKE_CURRENT_LIST_DIR}/my-toolchain-gcc.cmake)

add_link_options(-T "${TOOLCHAIN}/../snitch/sw/snRuntime/build-gcc/common.ld")

foreach(LANG  C CXX)
    set(CMAKE_${LANG}_STANDARD_LIBRARIES "${CMAKE_${LANG}_STANDARD_LIBRARIES} ${TOOLCHAIN}/../snitch/sw/snRuntime/build-gcc/libsnRuntime-banshee.a ${TOOLCHAIN}/../riscv64-unknown-elf-gcc/lib/gcc/riscv64-unknown-elf/8.3.0/rv32imfd/ilp32d/libgcc.a"
    )
    list(APPEND CMAKE_${LANG}_STANDARD_INCLUDE_DIRECTORIES 
        "${TOOLCHAIN}/../snitch/sw/snRuntime/include/"
        "${TOOLCHAIN}/../snitch/sw/vendor/riscv-opcodes/"
    )
endforeach()

