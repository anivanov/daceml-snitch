include(${CMAKE_CURRENT_LIST_DIR}/my-toolchain-llvm.cmake)


add_link_options(-T "${TOOLCHAIN}/../snitch/sw/snRuntime/build-llvm/common.ld")

foreach(LANG  C CXX)
    set(CMAKE_${LANG}_STANDARD_LIBRARIES "${CMAKE_${LANG}_STANDARD_LIBRARIES} ${TOOLCHAIN}/../snitch/sw/snRuntime/build-llvm/libsnRuntime-cluster.a"
    )
    list(APPEND CMAKE_${LANG}_STANDARD_INCLUDE_DIRECTORIES 
        "${TOOLCHAIN}/../snitch/sw/snRuntime/include/"
        "${TOOLCHAIN}/../snitch/sw/vendor/riscv-opcodes/"
    )
endforeach()