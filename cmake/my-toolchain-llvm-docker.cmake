# adapted from snitch/sw/cmake/toolchain-gcc.cmake

# Copyright 2020 ETH Zurich and University of Bologna.
# Solderpad Hardware License, Version 0.51, see LICENSE for details.
# SPDX-License-Identifier: SHL-0.51

set(TOOLCHAIN "/tools/riscv-llvm-install")

set(CMAKE_C_COMPILER "${TOOLCHAIN}/bin/clang")
set(CMAKE_CXX_COMPILER "${TOOLCHAIN}/bin/clang++")
set(CMAKE_OBJCOPY "${TOOLCHAIN}/bin/llvm-objcopy")
set(CMAKE_OBJDUMP "${TOOLCHAIN}/bin/llvm-objdump" --mcpu=snitch)
set(CMAKE_AR "${TOOLCHAIN}/bin/llvm-ar")
set(CMAKE_STRIP "${TOOLCHAIN}/bin/llvm-strip")
set(CMAKE_RANLIB "${TOOLCHAIN}/bin/llvm-ranlib")

# LTO
set(CMAKE_INTERPROCEDURAL_OPTIMIZATION true)
set(CMAKE_C_COMPILER_AR "${CMAKE_AR}")
set(CMAKE_CXX_COMPILER_AR "${CMAKE_AR}")
set(CMAKE_C_COMPILER_RANLIB "${CMAKE_RANLIB}")
set(CMAKE_CXX_COMPILER_RANLIB "${CMAKE_RANLIB}")

##
## Compile options
##
add_compile_options(-mcpu=snitch -mcmodel=medany -ffast-math -fno-builtin-printf -fno-common)
add_compile_options(-ffunction-sections)
add_compile_options(-Wextra)
add_compile_options(-static)
# For SSR register merge we need to disable the scheduler
add_compile_options(-mllvm -enable-misched=false)
# LLD doesn't support relaxation for RISC-V yet
add_compile_options(-mno-relax)
add_compile_options(-fopenmp)
# For smallfloat we need experimental extensions enabled (Zfh)
add_compile_options(-menable-experimental-extensions)
#add_compile_options(--sysroot="${TOOLCHAIN}/../riscv64-unknown-elf-gcc")

##
## Link options
##

add_link_options(-mcpu=snitch -nostartfiles -fuse-ld=lld -Wl,--image-base=0x80000000)
add_link_options(-nostdlib)
add_link_options(-static)
# LLD defaults to -z relro which we don't want in a static ELF
add_link_options(-Wl,-z,norelro)
add_link_options(-Wl,--gc-sections)
add_link_options(-Wl,--no-relax)
#add_link_options(-Wl,--verbose)

add_link_options(-nodefaultlibs)

# Libraries

foreach(LANG  C CXX)
    set(CMAKE_${LANG}_STANDARD_LIBRARIES 
        # C++ stdlib
        "/tools/riscv-gnu-install/riscv32-unknown-elf//lib/libstdc++.a"
        # C stdlib
        "/tools/newlib-install/riscv32-unknown-elf/lib/libc.a"
        "/tools/newlib-install/riscv32-unknown-elf/lib/libm.a"
        # GCC runtime
        "/tools/riscv-gnu-install/lib/gcc/riscv32-unknown-elf/12.2.0/libgcc.a"
        # Libffi
        "/tools/libffi-install/lib/libffi.a"
    )
    # convert ';' separated string to ' ' separated string
    list(JOIN CMAKE_${LANG}_STANDARD_LIBRARIES " " CMAKE_${LANG}_STANDARD_LIBRARIES)
    set(CMAKE_${LANG}_STANDARD_INCLUDE_DIRECTORIES 
        # C++ stdlib
        "/tools/riscv-gnu-install/riscv32-unknown-elf/include/c++/12.2.0/riscv32-unknown-elf/"
        "/tools/riscv-gnu-install/riscv32-unknown-elf/include/c++/12.2.0/"
        # C stdlib
        "/tools/newlib-install/riscv32-unknown-elf/include/"
        # GCC runtime
        "/tools/riscv-gnu-install/lib/gcc/riscv32-unknown-elf/12.2.0/include"
        # Libffi
        "/tools/libffi-install/include"
    )
endforeach()
