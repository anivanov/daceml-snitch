# adapted from snitch/sw/cmake/toolchain-gcc.cmake

# Copyright 2020 ETH Zurich and University of Bologna.
# Solderpad Hardware License, Version 0.51, see LICENSE for details.
# SPDX-License-Identifier: SHL-0.51

set(TOOLCHAIN "${CMAKE_CURRENT_LIST_DIR}/../gnu-toolchain/")
set(CMAKE_C_COMPILER "${TOOLCHAIN}/bin/riscv32-unknown-elf-gcc")
set(CMAKE_CXX_COMPILER "${TOOLCHAIN}/bin/riscv32-unknown-elf-g++")
set(CMAKE_OBJCOPY "${TOOLCHAIN}/bin/riscv32-unknown-elf-objcopy")
set(CMAKE_OBJDUMP "${TOOLCHAIN}/bin/riscv32-unknown-elf-objdump")
set(CMAKE_AR "${TOOLCHAIN}/bin/riscv32-unknown-elf-gcc-ar")
set(CMAKE_RANLIB "${TOOLCHAIN}/bin/riscv32-unknown-elf-gcc-ranlib")

# LTO
set(CMAKE_INTERPROCEDURAL_OPTIMIZATION true)
set(CMAKE_C_COMPILER_AR "${CMAKE_AR}")
set(CMAKE_CXX_COMPILER_AR "${CMAKE_AR}")
set(CMAKE_C_COMPILER_RANLIB "${CMAKE_RANLIB}")
set(CMAKE_CXX_COMPILER_RANLIB "${CMAKE_RANLIB}")


add_compile_options(-march=rv32imafd -mabi=ilp32d -mcmodel=medany -mno-fdiv -ffast-math -fno-builtin-printf -fno-common)
#add_compile_options(-march=rv32imafd -mabi=ilp32d -mcmodel=medany -mno-fdiv -ffast-math -fno-common)
add_link_options(-march=rv32imafd -mabi=ilp32d -nostartfiles -Wl,-Ttext-segment=0x80000000 )
#add_link_options(-Wl,--verbose)
add_link_options(-nodefaultlibs)
#add_link_options(-specs=nosys.specs)

add_compile_options(-nostdinc)

#add_compile_options(-ffunction-sections)
add_compile_options(-Wextra)

# Add preprocessor definition to indicate LD is used
add_compile_definitions(__LINK_LD)
add_compile_definitions(__TOOLCHAIN_GCC__)



foreach(LANG  C CXX)
    set(CMAKE_${LANG}_STANDARD_LIBRARIES 
        # C++ stdlib
        #"${TOOLCHAIN}/../gnu-toolchain/riscv32-unknown-elf/lib/libsupc++.a"
        "${TOOLCHAIN}/../gnu-toolchain/riscv32-unknown-elf/lib/libstdc++.a"
        # C++ runtime
        "${TOOLCHAIN}/../gnu-toolchain/lib/gcc/riscv32-unknown-elf/12.2.0/crtbegin.o"
        # C stdlib
        "${TOOLCHAIN}/../newlib-gcc12-root/riscv32-unknown-elf/lib/libc.a"
        "${TOOLCHAIN}/../newlib-gcc12-root/riscv32-unknown-elf/lib/libm.a"
        # GCC runtime
        #"${TOOLCHAIN}/../riscv64-unknown-elf-gcc/lib/gcc/riscv64-unknown-elf/8.3.0/rv32imfd/ilp32d/libgcc.a"
        "${TOOLCHAIN}/../gnu-toolchain/lib/gcc/riscv32-unknown-elf/12.2.0/libgcc.a"
    )
    # convert ';' separated string to ' ' separated string
    list(JOIN CMAKE_${LANG}_STANDARD_LIBRARIES " " CMAKE_${LANG}_STANDARD_LIBRARIES)
    set(CMAKE_${LANG}_STANDARD_INCLUDE_DIRECTORIES 
        # C++ stdlib
        "${TOOLCHAIN}/../gnu-toolchain/riscv32-unknown-elf/include/c++/12.2.0/riscv32-unknown-elf/"
        "${TOOLCHAIN}/../gnu-toolchain/riscv32-unknown-elf/include/c++/12.2.0/"
        # C stdlib
        "${TOOLCHAIN}/../newlib-gcc12-root/riscv32-unknown-elf/include/"
        # GCC runtime
        #"${TOOLCHAIN}/../riscv64-unknown-elf-gcc/lib/gcc/riscv64-unknown-elf/8.3.0/include/"
        "${TOOLCHAIN}/../gnu-toolchain/lib/gcc/riscv32-unknown-elf/12.2.0/include"
    )
endforeach()