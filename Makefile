NPROC ?= $(shell nproc)
VERILATOR := snitch/hw/system/snitch_cluster/bin/snitch_cluster.vlt
BANSHEE := snitch/sw/banshee/target/debug/banshee
COMPILER_RT := toolchain/lib/clang/12.0.1/lib/libclang_rt.builtins-riscv32.a
LLVM_COMPILER := toolchain/bin/clang
GCC_COMPILER := riscv64-unknown-elf-gcc/bin/riscv64-unknown-elf-gcc
ALL_TOOLS := $(VERILATOR) $(BANSHEE) $(LLVM_COMPILER) $(COMPILER_RT) $(GCC_COMPILER) venv/bin/activate venv/.timestamp-snitch venv/.timestamp-dace venv/.timestamp-daceml
DOCKER ?= podman
DOCKER_OPTS ?= --security-opt label=disable --rm --cap-add=SYS_PTRACE
DOCKER_RUN ?= $(DOCKER) run $(DOCKER_OPTS)
DOCKER_IMG ?= ghcr.io/pulp-platform/snitch_cluster:main@sha256:7c80c129d29b15778d09a5a8c7e74b3a6b1f7b2f6e59a8d96f4cf11a6fb41447


.PHONY: tools
tools: $(ALL_TOOLS)

.PHONY: run-examples-llvm
run-examples-llvm: run-verilator-hello_world run-banshee-hello_world run-banshee-snBLAS-simple run-verilator-benchmark-matmul-all run-banshee-benchmark-matmul-all run-banshee-snBLAS-simple

.PHONY: run-examples-gcc
run-examples-gcc: run-verilator-hello_world-gcc run-verilator-benchmark-matmul-all-gcc

# START OF TOOLING SETUP

dace/README.md daceml/README.md newlib/README pulp-llvm/README.md snitch/README.md riscv-gnu-toolchain/README.md:
	git submodule update --init --recursive

# X86 target here is required only to build banshee
pulp-llvm-build/CMakeCache.txt: pulp-llvm/README.md
	mkdir -p pulp-llvm-build
	cd "pulp-llvm-build" && cmake -DCMAKE_BUILD_TYPE="Release" \
		-DLLVM_ENABLE_PROJECTS="clang;lld" \
		-DLLVM_TARGETS_TO_BUILD="RISCV" \
		-DLLVM_DEFAULT_TARGET_TRIPLE="riscv32-unknown-elf" \
		-DLLVM_OPTIMIZED_TABLEGEN=True \
		-DCMAKE_INSTALL_PREFIX="${CURDIR}/toolchain" \
		"${CURDIR}/pulp-llvm/llvm"

pulp-llvm-build/bin/clang: pulp-llvm-build/CMakeCache.txt
	cmake --build pulp-llvm-build -j $(NPROC)

$(LLVM_COMPILER): pulp-llvm-build/bin/clang riscv64-unknown-elf-gcc/bin/riscv64-unknown-elf-gcc
	cmake --install pulp-llvm-build

# newlib llvm

newlib-llvm-build/Makefile: newlib/README
	mkdir -p newlib-llvm-build
	cd newlib-llvm-build && "${CURDIR}/newlib/configure" \
		--target=riscv32-unknown-elf \
		--prefix="${CURDIR}/newlib-llvm-root" \
		--enable-newlib-retargetable-locking \
		AR_FOR_TARGET="${CURDIR}/toolchain/bin/llvm-ar" \
		AS_FOR_TARGET="${CURDIR}/toolchain/bin/llvm-as" \
		LD_FOR_TARGET="${CURDIR}/toolchain/bin/llvm-ld" \
		RANLIB_FOR_TARGET="${CURDIR}/toolchain/bin/llvm-ranlib" \
		CC_FOR_TARGET="${CURDIR}/toolchain/bin/clang" \
		CFLAGS_FOR_TARGET="-march=rv32imafd -mabi=ilp32d -O2" \
		CXXFLAGS_FOR_TARGET="-march=rv32imafd -mabi=ilp32d -O2"

newlib-llvm-build/riscv32-unknown-elf/newlib/libc.a: newlib-llvm-build/Makefile
	$(MAKE) -C newlib-llvm-build -j $(NPROC)

newlib-llvm-root/riscv32-unknown-elf/lib/libc.a: newlib-llvm-build/riscv32-unknown-elf/newlib/libc.a
	$(MAKE) -C newlib-llvm-build install

# newlib gcc

newlib-gcc-build/Makefile: newlib/README
	mkdir -p newlib-gcc-build
	cd newlib-gcc-build && "${CURDIR}/newlib/configure" \
		--target=riscv32-unknown-elf \
		--prefix="${CURDIR}/newlib-gcc-root" \
		--enable-newlib-retargetable-locking \
		AR_FOR_TARGET="${CURDIR}/riscv64-unknown-elf-gcc/bin/riscv64-unknown-elf-gcc-ar" \
		AS_FOR_TARGET="${CURDIR}/riscv64-unknown-elf-gcc/bin/riscv64-unknown-elf-as" \
		LD_FOR_TARGET="${CURDIR}/riscv64-unknown-elf-gcc/bin/riscv64-unknown-elf-ld" \
		RANLIB_FOR_TARGET="${CURDIR}/riscv64-unknown-elf-gcc/bin/riscv64-unknown-elf-gcc-ranlib" \
		CC_FOR_TARGET="${CURDIR}/riscv64-unknown-elf-gcc/bin/riscv64-unknown-elf-gcc" \
		CFLAGS_FOR_TARGET="-march=rv32imafd -mabi=ilp32d -O3 -g" \
		CXXFLAGS_FOR_TARGET="-march=rv32imafd -mabi=ilp32d -O3 -g"

newlib-gcc-build/riscv32-unknown-elf/rv32imfd/ilp32d/newlib/libc.a: newlib-gcc-build/Makefile
	$(MAKE) -C newlib-gcc-build -j $(NPROC)

newlib-gcc-root/riscv32-unknown-elf/lib/rv32imfd/ilp32d/libc.a: newlib-gcc-build/riscv32-unknown-elf/rv32imfd/ilp32d/newlib/libc.a
	$(MAKE) -C newlib-gcc-build install

# newlib gcc12

newlib-gcc12-build/Makefile: newlib/README
	mkdir -p newlib-gcc12-build
	cd newlib-gcc12-build && "${CURDIR}/newlib/configure" \
		--target=riscv32-unknown-elf \
		--prefix="${CURDIR}/newlib-gcc12-root" \
		--enable-newlib-retargetable-locking \
		AR_FOR_TARGET="${CURDIR}/gnu-toolchain/bin/riscv32-unknown-elf-gcc-ar" \
		AS_FOR_TARGET="${CURDIR}/gnu-toolchain/bin/riscv32-unknown-elf-as" \
		LD_FOR_TARGET="${CURDIR}/gnu-toolchain/bin/riscv32-unknown-elf-ld" \
		RANLIB_FOR_TARGET="${CURDIR}/gnu-toolchain/bin/riscv32-unknown-elf-gcc-ranlib" \
		CC_FOR_TARGET="${CURDIR}/gnu-toolchain/bin/riscv32-unknown-elf-gcc" \
		CFLAGS_FOR_TARGET="-march=rv32imafd -mabi=ilp32d -O3 -g" \
		CXXFLAGS_FOR_TARGET="-march=rv32imafd -mabi=ilp32d -O3 -g"

newlib-gcc12-build/riscv32-unknown-elf/newlib/libc.a: newlib-gcc12-build/Makefile
	$(MAKE) -C newlib-gcc12-build -j $(NPROC)

newlib-gcc12-root/riscv32-unknown-elf/lib/libc.a: newlib-gcc12-build/riscv32-unknown-elf/newlib/libc.a
	$(MAKE) -C newlib-gcc12-build install

# -----------

compiler-rt-build/CMakeCache.txt: toolchain/bin/clang toolchain/riscv32-unknown-elf/lib/libc.a
	mkdir -p compiler-rt-build
	cd "compiler-rt-build" && cmake \
		-DCMAKE_SYSTEM_NAME=Linux \
		-DCMAKE_INSTALL_PREFIX="$(shell ${CURDIR}/toolchain/bin/clang -print-resource-dir)" \
		-DCMAKE_C_COMPILER="${CURDIR}/toolchain/bin/clang" \
		-DCMAKE_CXX_COMPILER="${CURDIR}/toolchain/bin/clang" \
		-DCMAKE_ASM_COMPILER="${CURDIR}/toolchain/bin/clang" \
		-DCMAKE_AR="${CURDIR}/toolchain/bin/llvm-ar" \
		-DCMAKE_NM="${CURDIR}/toolchain/bin/llvm-nm" \
		-DCMAKE_RANLIB="${CURDIR}/toolchain/bin/llvm-ranlib" \
		-DCMAKE_C_COMPILER_TARGET="riscv32-unknown-elf" \
		-DCMAKE_CXX_COMPILER_TARGET="riscv32-unknown-elf" \
		-DCMAKE_ASM_COMPILER_TARGET="riscv32-unknown-elf" \
		-DCMAKE_C_FLAGS="-march=rv32imafd -mabi=ilp32d" \
		-DCMAKE_CXX_FLAGS="-march=rv32imafd -mabi=ilp32d" \
		-DCMAKE_ASM_FLAGS="-march=rv32imafd -mabi=ilp32d" \
		-DCMAKE_EXE_LINKER_FLAGS="-nostartfiles -nostdlib -fuse-ld=lld" \
		-DCOMPILER_RT_BAREMETAL_BUILD=ON \
		-DCOMPILER_RT_BUILD_BUILTINS=ON \
		-DCOMPILER_RT_BUILD_MEMPROF=OFF \
		-DCOMPILER_RT_BUILD_LIBFUZZER=OFF \
		-DCOMPILER_RT_BUILD_PROFILE=OFF \
		-DCOMPILER_RT_BUILD_SANITIZERS=OFF \
		-DCOMPILER_RT_BUILD_XRAY=OFF \
		-DCOMPILER_RT_DEFAULT_TARGET_ONLY=ON \
		-DCOMPILER_RT_OS_DIR="" \
		-DLLVM_CONFIG_PATH="${CURDIR}/pulp-llvm-build/bin/llvm-config" \
		"${CURDIR}/pulp-llvm/compiler-rt"

compiler-rt-build/lib/libclang_rt.builtins-riscv32.a: compiler-rt-build/CMakeCache.txt
	cmake --build "compiler-rt-build" -j $(NPROC) && touch $@

$(COMPILER_RT): compiler-rt-build/lib/libclang_rt.builtins-riscv32.a
	cmake --install "compiler-rt-build" && touch $@

venv/.timestamp-snitch: venv/bin/activate snitch/README.md
	. venv/bin/activate && pip install -r snitch/python-requirements.txt
	touch $@

venv/bin/activate:
	python -m venv venv

$(VERILATOR): snitch/README.md venv/.timestamp-snitch
	. venv/bin/activate && $(MAKE) -C snitch/hw/system/snitch_cluster bin/snitch_cluster.vlt -j $(NPROC)

$(BANSHEE): $(LLVM_COMPILER) $(COMPILER_RT)
	cd snitch/sw/banshee && LLVM_SYS_120_PREFIX="${CURDIR}/toolchain/" cargo build

venv/.timestamp-daceml: venv/bin/activate daceml/README.md 
	. venv/bin/activate && pip install -e ./daceml
	touch $@

# install AFTER daceml
venv/.timestamp-dace: venv/bin/activate dace/README.md venv/.timestamp-daceml
	. venv/bin/activate && pip install -e ./dace
	touch $@


riscv64-unknown-elf-gcc.tar.gz:
	wget https://static.dev.sifive.com/dev-tools/riscv64-unknown-elf-gcc-8.3.0-2020.04.0-x86_64-linux-ubuntu14.tar.gz -O riscv64-unknown-elf-gcc.tar.gz

$(GCC_COMPILER): riscv64-unknown-elf-gcc.tar.gz
	mkdir -p riscv64-unknown-elf-gcc && tar xf riscv64-unknown-elf-gcc.tar.gz --strip-components=1 --touch -C riscv64-unknown-elf-gcc

# END OF TOOLING SETUP


.PHONY: test-banshee
test-banshee: $(BANSHEE)
	LLVM_SYS_120_PREFIX="${CURDIR}/toolchain/" make -C snitch/sw/banshee test

.PHONY: build-verilator-hello_world
build-verilator-hello_world: tools venv/.timestamp-snitch
	mkdir -p snitch/sw/examples/build-cluster
	cd snitch/sw/examples/build-cluster && cmake \
		-DCMAKE_TOOLCHAIN_FILE="${CURDIR}/cmake/my-toolchain-llvm.cmake" \
		-DSNITCH_RUNTIME=snRuntime-cluster \
		"${CURDIR}/snitch/sw/examples"
	cmake --build snitch/sw/examples/build-cluster -j $(NPROC) --target hello_world

.PHONY: build-banshee-hello_world
build-banshee-hello_world: tools venv/.timestamp-snitch
	mkdir -p snitch/sw/examples/build-banshee
	cd snitch/sw/examples/build-banshee && cmake \
		-DCMAKE_TOOLCHAIN_FILE="${CURDIR}/cmake/my-toolchain-llvm.cmake" \
		-DSNITCH_RUNTIME=snRuntime-banshee \
		"${CURDIR}/snitch/sw/examples"
	cmake --build snitch/sw/examples/build-banshee -j $(NPROC) --target hello_world

.PHONY: run-verilator-hello_world
run-verilator-hello_world: build-verilator-hello_world $(VERILATOR)
	$(VERILATOR) snitch/sw/examples/build-cluster/hello_world

.PHONY: run-banshee-hello_world
run-banshee-hello_world: build-banshee-hello_world $(BANSHEE)
	$(BANSHEE) --configuration snitch/sw/banshee/config/snitch_cluster.yaml snitch/sw/examples/build-banshee/hello_world

snitch/sw/benchmark/build-cluster/benchmark-matmul-all: tools venv/.timestamp-snitch
	mkdir -p snitch/sw/benchmark/build-cluster
	cd snitch/sw/benchmark/build-cluster && cmake \
		-DCMAKE_TOOLCHAIN_FILE="${CURDIR}/cmake/my-toolchain-llvm.cmake" \
		-DSNITCH_RUNTIME=snRuntime-cluster \
		"${CURDIR}/snitch/sw/benchmark"
	cmake --build snitch/sw/benchmark/build-cluster -j $(NPROC) --target benchmark-matmul-all

snitch/sw/benchmark/build-banshee/benchmark-matmul-all: tools venv/.timestamp-snitch
	mkdir -p snitch/sw/benchmark/build-banshee
	cd snitch/sw/benchmark/build-banshee && cmake \
		-DCMAKE_TOOLCHAIN_FILE="${CURDIR}/cmake/my-toolchain-llvm.cmake" \
		-DSNITCH_RUNTIME=snRuntime-banshee \
		"${CURDIR}/snitch/sw/benchmark"
	cmake --build snitch/sw/benchmark/build-banshee -j $(NPROC) --target benchmark-matmul-all

.PHONY: run-verilator-benchmark-matmul-all
run-verilator-benchmark-matmul-all: snitch/sw/benchmark/build-cluster/benchmark-matmul-all $(VERILATOR)
	$(VERILATOR) snitch/sw/benchmark/build-cluster/benchmark-matmul-all

.PHONY: run-banshee-benchmark-matmul-all
run-banshee-benchmark-matmul-all: snitch/sw/benchmark/build-banshee/benchmark-matmul-all $(BANSHEE)
	$(BANSHEE) --configuration snitch/sw/banshee/config/snitch_cluster.yaml snitch/sw/benchmark/build-banshee/benchmark-matmul-all

snitch/sw/snBLAS/build-cluster/test-snBLAS-simple: tools venv/.timestamp-snitch
	mkdir -p snitch/sw/snBLAS/build-cluster
	cd snitch/sw/snBLAS/build-cluster && cmake \
		-DCMAKE_TOOLCHAIN_FILE="${CURDIR}/cmake/my-toolchain-llvm.cmake" \
		-DSNITCH_RUNTIME=snRuntime-cluster \
		-DBUILD_TESTS=ON \
		"${CURDIR}/snitch/sw/snBLAS"
	cmake --build snitch/sw/snBLAS/build-cluster -j $(NPROC) --target test-snBLAS-simple

snitch/sw/snBLAS/build-banshee/test-snBLAS-simple: tools venv/.timestamp-snitch
	mkdir -p snitch/sw/snBLAS/build-banshee
	cd snitch/sw/snBLAS/build-banshee && cmake \
		-DCMAKE_TOOLCHAIN_FILE="${CURDIR}/cmake/my-toolchain-llvm.cmake" \
		-DSNITCH_RUNTIME=snRuntime-banshee \
		-DBUILD_TESTS=ON \
		"${CURDIR}/snitch/sw/snBLAS"
	cmake --build snitch/sw/snBLAS/build-banshee -j $(NPROC) --target test-snBLAS-simple

# this target is known to be broken, it hangs with [Illegal Instruction Core 8] PC: 000080000880 Data: d2140053
#.PHONY: run-verilator-snBLAS-simple
#run-verilator-snBLAS-simple: snitch/sw/snBLAS/build-cluster/test-snBLAS-simple $(VERILATOR)
#	$(VERILATOR) snitch/sw/snBLAS/build-cluster/test-snBLAS-simple

.PHONY: run-banshee-snBLAS-simple
run-banshee-snBLAS-simple: snitch/sw/snBLAS/build-banshee/test-snBLAS-simple $(BANSHEE)
	$(BANSHEE) --configuration snitch/sw/banshee/config/snitch_cluster.yaml snitch/sw/snBLAS/build-banshee/test-snBLAS-simple

snitch/sw/examples/build-cluster-gcc/hello_world: venv/.timestamp-snitch riscv64-unknown-elf-gcc/bin/riscv64-unknown-elf-gcc
	mkdir -p snitch/sw/examples/build-cluster-gcc
	cd snitch/sw/examples/build-cluster-gcc && cmake \
		-DCMAKE_TOOLCHAIN_FILE="${CURDIR}/cmake/my-toolchain-gcc.cmake" \
		-DSNITCH_RUNTIME=snRuntime-cluster \
		"${CURDIR}/snitch/sw/examples"
	cmake --build snitch/sw/examples/build-cluster-gcc -j $(NPROC) --target hello_world

.PHONY: run-verilator-hello_world-gcc
run-verilator-hello_world-gcc: snitch/sw/examples/build-cluster-gcc/hello_world $(VERILATOR)
	$(VERILATOR) snitch/sw/examples/build-cluster-gcc/hello_world

snitch/sw/benchmark/build-cluster-gcc/benchmark-matmul-all: venv/.timestamp-snitch riscv64-unknown-elf-gcc/bin/riscv64-unknown-elf-gcc
	mkdir -p snitch/sw/benchmark/build-cluster-gcc
	cd snitch/sw/benchmark/build-cluster-gcc && cmake \
		-DCMAKE_TOOLCHAIN_FILE="${CURDIR}/cmake/my-toolchain-gcc.cmake" \
		-DSNITCH_RUNTIME=snRuntime-cluster \
		"${CURDIR}/snitch/sw/benchmark"
	cmake --build snitch/sw/benchmark/build-cluster-gcc -j $(NPROC) --target benchmark-matmul-all

.PHONY: run-verilator-benchmark-matmul-all-gcc
run-verilator-benchmark-matmul-all-gcc: snitch/sw/benchmark/build-cluster-gcc/benchmark-matmul-all $(VERILATOR)
	$(VERILATOR) snitch/sw/benchmark/build-cluster-gcc/benchmark-matmul-all


snitch/sw/snRuntime/build/common.ld \
snitch/sw/snRuntime/build/libsnRuntime.a \
snitch/sw/snRuntime/build/libsnRuntime-banshee.a \
snitch/sw/snRuntime/build/libsnRuntime-cluster.a \
: riscv64-unknown-elf-gcc/bin/riscv64-unknown-elf-gcc snitch/README.md venv/.timestamp-snitch
	mkdir -p snitch/sw/snRuntime/build
	cd snitch/sw/snRuntime/build && cmake .. \
		-DCMAKE_TOOLCHAIN_FILE="${CURDIR}/cmake/my-toolchain-llvm.cmake"
	cmake --build snitch/sw/snRuntime/build/

.PHONY: build-snRuntime
build-snRuntime: venv/.timestamp-snitch tools
	mkdir -p snitch/sw/snRuntime/build-cluster
	cd snitch/sw/snRuntime/build-cluster && cmake \
		-DCMAKE_TOOLCHAIN_FILE="${CURDIR}/cmake/my-toolchain-llvm.cmake" \
		-DSNITCH_RUNTIME=snRuntime-cluster \
		-DBUILD_TESTS=ON \
		"${CURDIR}/snitch/sw/snRuntime"
	cmake --build snitch/sw/snRuntime/build-cluster -j $(NPROC)

.PHONY: test-snRuntime-openmp
test-snRuntime-openmp: build-snRuntime $(VERILATOR)
	$(VERILATOR) snitch/sw/snRuntime/build-cluster/test-snRuntime-openmp

# START snitch runtime

snitch/sw/snRuntime/build-gcc/libsnRuntime.a: venv/.timestamp-snitch tools
	mkdir -p snitch/sw/snRuntime/build-gcc
	cd snitch/sw/snRuntime/build-gcc && cmake \
		-DCMAKE_TOOLCHAIN_FILE="${CURDIR}/cmake/my-toolchain-gcc.cmake" \
		-DBUILD_TESTS=ON \
		"${CURDIR}/snitch/sw/snRuntime"
	cmake --build snitch/sw/snRuntime/build-gcc -j $(NPROC)

snitch/sw/snRuntime/build-gcc12/libsnRuntime.a: venv/.timestamp-snitch tools
	mkdir -p snitch/sw/snRuntime/build-gcc12
	cd snitch/sw/snRuntime/build-gcc12 && cmake \
		-DCMAKE_TOOLCHAIN_FILE="${CURDIR}/cmake/my-toolchain-gcc12.cmake" \
		-DBUILD_TESTS=ON \
		"${CURDIR}/snitch/sw/snRuntime"
	cmake --build snitch/sw/snRuntime/build-gcc12 -j $(NPROC)

snitch/sw/snRuntime/build-llvm/libsnRuntime.a: venv/.timestamp-snitch tools
	mkdir -p snitch/sw/snRuntime/build-llvm
	cd snitch/sw/snRuntime/build-llvm && cmake \
		-DCMAKE_TOOLCHAIN_FILE="${CURDIR}/cmake/my-toolchain-llvm.cmake" \
		-DBUILD_TESTS=ON \
		"${CURDIR}/snitch/sw/snRuntime"
	cmake --build snitch/sw/snRuntime/build-llvm -j $(NPROC)

# END snitch runtime

.PHONY: test-snRuntime-openmp-banshee
test-snRuntime-openmp-banshee: build-snRuntime-banshee $(BANSHEE)
	$(BANSHEE) --configuration snitch/sw/banshee/config/snitch_cluster.yaml snitch/sw/snRuntime/build-banshee/test-snRuntime-openmp

# gcc 12

gnu-toolchain/bin/riscv32-unknown-elf-gcc : riscv-gnu-toolchain/README.md
	cd riscv-gnu-toolchain && ./configure --prefix="${CURDIR}/gnu-toolchain" --with-arch=rv32imafd --with-abi=ilp32d && make -j $(NPROC)


# things to run through docker

matmul-banshee/benchmark-matmul-all:
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo -w /repo ghcr.io/pulp-platform/snitch /bin/bash -c "\
		mkdir -p /repo/matmul-banshee && \
		cd /repo/matmul-banshee && cmake \
			-DCMAKE_TOOLCHAIN_FILE=/repo/snitch/sw/cmake/toolchain-llvm.cmake \
			-DSNITCH_RUNTIME=snRuntime-banshee \
			/repo/snitch/sw/benchmark && \
		cmake --build . -j `nproc` --target benchmark-matmul-all"


.PHONY: run-matmul-banshee
run-matmul-banshee: matmul-banshee/benchmark-matmul-all
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo -w /repo ghcr.io/pulp-platform/snitch /bin/bash -c "\
		banshee --configuration /repo/snitch/sw/banshee/config/snitch_cluster.yaml --latency \
			/repo/matmul-banshee/benchmark-matmul-all"


snitch_cluster.vlt: _dockerimg
	$(DOCKER_RUN)  \
		--mount type=bind,source=`pwd`,target=/repo \
		-w /repo \
		spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest /bin/bash -c "\
		mkdir /workspace && cd /workspace && \
		git clone https://github.com/pulp-platform/snitch_cluster.git && \
		cd snitch_cluster && \
		git checkout 11d0534 && \
		sed -i -e 's;wget;wget --no-check-certificate;g' target/common/common.mk && \
		VLT_BENDER=-DCOMMON_CELLS_ASSERTS_OFF CFG_OVERRIDE=cfg/fdiv.hjson \
		make -C target/snitch_cluster bin/snitch_cluster.vlt && \
		cp target/snitch_cluster/bin/snitch_cluster.vlt /repo/snitch_cluster.vlt"


snRuntime-gcc-build/libsnRuntime-cluster.a snRuntime-gcc-build/libsnRuntime-banshee.a: _dockerimg
	$(DOCKER_RUN)  \
		--mount type=bind,source=`pwd`,target=/repo \
		-w /repo \
		spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest \
		/bin/bash -c "\
			rm -rf /repo/snRuntime-gcc-build/ && \
			mkdir -p /repo/snRuntime-gcc-build && \
			cd /repo/snRuntime-gcc-build && \
			cmake /repo/snitch/sw/snRuntime \
				-DSNITCH_BANSHEE=/repo/banshee \
				-DSNITCH_SIMULATOR=/repo/snitch_cluster.vlt \
				-DBUILD_TESTS=ON \
				-DSNITCH_RUNTIME=snRuntime-cluster \
				-DCMAKE_TOOLCHAIN_FILE=toolchain-gcc && \
			cmake --build . -j `nproc`"


gcc_div.a: gcc_div.c _dockerimg
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo --mount=type=bind,source=`pwd`/daceml,destination=/tools/daceml -w /repo spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest /bin/bash -c "\
		/tools/riscv/bin/riscv64-unknown-elf-gcc \
			-march=rv32imafd -mabi=ilp32d -mcmodel=medany -ffast-math -fno-builtin-printf -fno-common \
			-nostdinc -ffixed-ft0 -ffixed-ft1 -ffixed-ft2 -march=rv32imafd -mabi=ilp32d \
			-nostartfiles -Wl,-Ttext-segment=0x80000000 -nodefaultlibs -ffunction-sections \
			/repo/gcc_div.c -c  -o /repo/gcc_div.o && \
		/tools/riscv/bin/riscv64-unknown-elf-gcc-ar rcs /repo/gcc_div.a /repo/gcc_div.o"

banshee-target/debug/banshee: _dockerimg
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest /bin/bash -c "\
		cd /repo/snitch/sw/banshee && rm -rf /repo/banshee-target/ && CARGO_TARGET_DIR=/repo/banshee-target/ cargo build"


matmul-verilator/benchmark-matmul-all:
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo -w /repo ghcr.io/pulp-platform/snitch /bin/bash -c "\
		mkdir -p /repo/matmul-verilator && \
		cd /repo/matmul-verilator && cmake \
			-DCMAKE_TOOLCHAIN_FILE=/repo/snitch/sw/cmake/toolchain-llvm.cmake \
			-DSNITCH_RUNTIME=snRuntime-cluster \
			/repo/snitch/sw/benchmark && \
		cmake --build . -j `nproc` --target benchmark-matmul-all"


.PHONY: run-matmul-verilator
run-matmul-verilator: matmul-verilator/benchmark-matmul-all snitch_cluster.vlt
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo -w /repo ghcr.io/pulp-platform/snitch /bin/bash -c "\
		./snitch_cluster.vlt ./matmul-verilator/benchmark-matmul-all"


dphpc_lmq_build/benchmark_abs:
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo -w /repo ghcr.io/pulp-platform/snitch /bin/bash -c "\
		mkdir -p /repo/dphpc_lmq_build && \
		cd /repo/dphpc_lmq_build && cmake -DCMAKE_TOOLCHAIN_FILE=/repo/snitch/sw/cmake/toolchain-llvm.cmake -DSNITCH_SOFTWARE_DIR=/repo/snitch/sw /repo/dphpc_lmq && cmake --build . -j `nproc`"


.PHONY: run-lmq-benchmark_abs
run-lmq-benchmark_abs: dphpc_lmq_build/benchmark_abs
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo -w /repo ghcr.io/pulp-platform/snitch /bin/bash -c "\
		banshee --configuration /repo/snitch/sw/banshee/config/snitch_cluster.yaml \
			--latency /repo/dphpc_lmq_build/benchmark_abs"


_dockerimg: Dockerfile
	# ${DOCKER} build -t spclgitlab.ethz.ch/anivanov/daceml-snitch .
	${DOCKER} pull spclgitlab.ethz.ch:5050/anivanov/daceml-snitch
	touch _dockerimg


run-img1: _dockerimg
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo --mount=type=bind,source=`pwd`/daceml,destination=/tools/daceml -w /repo spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest python /repo/samples/yolov5_onnx.py


run-img: _dockerimg
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo --mount=type=bind,source=`pwd`/daceml,destination=/tools/daceml -w /repo spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest /bin/bash


daceml_container/timestamp: daceml_container/Dockerfile
	${DOCKER} build -t daceml daceml_container
	touch $@


onnx-sparse-banshee-build/matmul_csr_dense: _dockerimg
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo -w /repo spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest /bin/bash -c "\
		mkdir -p /repo/onnx-sparse-banshee-build && \
		cd /repo/onnx-sparse-banshee-build && cmake \
			-DCMAKE_TOOLCHAIN_FILE=/tools/toolchain-llvm.cmake \
			-DSNITCH_RUNTIME=snRuntime-banshee \
			/repo/snitch/sw/dphpc && \
		cmake --build . -j `nproc`"


# onnx-sparse-banshee-build/matmul_csr_dense: _dockerimg
# 	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo -w /repo spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest2 /bin/bash -c "\
# 		mkdir -p /repo/onnx-sparse-banshee-build && \
# 		cd /repo/onnx-sparse-banshee-build && cmake \
# 			-DCMAKE_TOOLCHAIN_FILE=/repo/snitch/sw/cmake/toolchain-llvm.cmake \
# 			-DSNITCH_RUNTIME=snRuntime-banshee \
# 			/repo/snitch/sw/dphpc && \
# 		cmake --build . -j `nproc`"


.PHONY: run-matmul_csr_dense
run-matmul_csr_dense: onnx-sparse-banshee-build/matmul_csr_dense
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo -w /repo ghcr.io/pulp-platform/snitch /bin/bash -c "\
		banshee --configuration /repo/snitch/sw/banshee/config/snitch_cluster.yaml \
			--latency /repo/onnx-sparse-banshee-build/matmul_csr_dense"


.PHONY: run-matmul_csr_csr
run-matmul_csr_csr: onnx-sparse-banshee-build/matmul_csr_dense
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo -w /repo ghcr.io/pulp-platform/snitch /bin/bash -c "\
		banshee --configuration /repo/snitch/sw/banshee/config/snitch_cluster.yaml \
			--latency /repo/onnx-sparse-banshee-build/matmul_csr_csr"


.PHONY: run-matmul_dense_dense
run-matmul_dense_dense: onnx-sparse-banshee-build/matmul_csr_dense
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo -w /repo ghcr.io/pulp-platform/snitch /bin/bash -c "\
		banshee --configuration /repo/snitch/sw/banshee/config/snitch_cluster.yaml \
			--latency /repo/onnx-sparse-banshee-build/matmul_dense_dense"


.PHONY: run-conv2d_csr_csr_csr
run-conv2d_csr_csr_csr: onnx-sparse-banshee-build/matmul_csr_dense
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo -w /repo ghcr.io/pulp-platform/snitch /bin/bash -c "\
		banshee --configuration /repo/snitch/sw/banshee/config/snitch_cluster.yaml \
			--latency /repo/onnx-sparse-banshee-build/conv2d_csr_csr_csr"


.PHONY: run-softmax_csr
run-softmax_csr: onnx-sparse-banshee-build/matmul_csr_dense
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo -w /repo ghcr.io/pulp-platform/snitch /bin/bash -c "\
		banshee --configuration /repo/snitch/sw/banshee/config/snitch_cluster.yaml \
			--latency /repo/onnx-sparse-banshee-build/softmax_csr"


.PHONY: run-conv2d_csr_dense_csr
run-conv2d_csr_dense_csr: onnx-sparse-banshee-build/matmul_csr_dense
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo -w /repo ghcr.io/pulp-platform/snitch /bin/bash -c "\
		banshee --configuration /repo/snitch/sw/banshee/config/snitch_cluster.yaml \
			--latency /repo/onnx-sparse-banshee-build/conv2d_csr_dense_csr"


.PHONY: run-matmul_csr_dense_to_dense
run-matmul_csr_dense_to_dense: onnx-sparse-banshee-build/matmul_csr_dense
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo -w /repo ghcr.io/pulp-platform/snitch /bin/bash -c "\
		banshee --configuration /repo/snitch/sw/banshee/config/snitch_cluster.yaml \
			--latency /repo/onnx-sparse-banshee-build/matmul_csr_dense_to_dense"

run-roberta: _dockerimg snRuntime-gcc-build/libsnRuntime-banshee.a gcc_div.a
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo --mount=type=bind,source=`pwd`/dace,destination=/tools/dace --mount=type=bind,source=`pwd`/daceml,destination=/tools/daceml -w /repo spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest python -m pdb -c c /repo/samples/roberta.py --simulator banshee

run-mlp_simple: _dockerimg snRuntime-gcc-build/libsnRuntime-banshee.a gcc_div.a
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo --mount=type=bind,source=`pwd`/daceml,destination=/tools/daceml -w /repo spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest python /repo/samples/mlp_simple.py --simulator banshee

run-mlp_simple-vlt: _dockerimg snRuntime-gcc-build/libsnRuntime-cluster.a gcc_div.a snitch_cluster.vlt
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo --mount=type=bind,source=`pwd`/daceml,destination=/tools/daceml -w /repo spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest python /repo/samples/mlp_simple.py --simulator verilator

run-transformer: _dockerimg snRuntime-gcc-build/libsnRuntime-banshee.a gcc_div.a
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo --mount=type=bind,source=`pwd`/daceml,destination=/tools/daceml -w /repo spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest python /repo/samples/transformer.py --simulator banshee

run-transformer-vlt: _dockerimg snRuntime-gcc-build/libsnRuntime-cluster.a gcc_div.a snitch_cluster.vlt
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo --mount=type=bind,source=`pwd`/daceml,destination=/tools/daceml -w /repo spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest python /repo/samples/transformer.py --simulator verilator

run-yolov5: _dockerimg snRuntime-gcc-build/libsnRuntime-banshee.a gcc_div.a
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo --mount=type=bind,source=`pwd`/daceml,destination=/tools/daceml -w /repo spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest python /repo/samples/yolov5.py --simulator banshee

run-yolov5-vlt: _dockerimg snRuntime-gcc-build/libsnRuntime-cluster.a gcc_div.a snitch_cluster.vlt
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo --mount=type=bind,source=`pwd`/daceml,destination=/tools/daceml -w /repo spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest python /repo/samples/yolov5.py --simulator verilator

run-yolov5-onnx: _dockerimg snRuntime-gcc-build/libsnRuntime-banshee.a gcc_div.a
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo --mount=type=bind,source=`pwd`/daceml,destination=/tools/daceml -w /repo spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest python /repo/samples/yolov5_onnx.py --simulator banshee

run-yolov5-onnx-vlt: _dockerimg snRuntime-gcc-build/libsnRuntime-cluster.a gcc_div.a snitch_cluster.vlt
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`,destination=/repo --mount=type=bind,source=`pwd`/daceml,destination=/tools/daceml -w /repo spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest python /repo/samples/yolov5_onnx.py --simulator verilator


run-layernorm: _dockerimg snRuntime-build/common.ld banshee-target/debug/banshee snitch_cluster.vlt
	$(DOCKER_RUN) \
		--mount=type=bind,source=`pwd`,destination=/repo \
		--mount=type=bind,source=`pwd`/dace,destination=/tools/dace \
		--mount=type=bind,source=`pwd`/daceml,destination=/tools/daceml \
		$(DOCKER_OPTS) \
		-w /repo \
		spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest \
		/bin/bash -c "\
			RUST_MIN_STACK=134217728 \
			SNITCH_LOG= \
			SNVLT_PATH=/repo/snitch_cluster.vlt \
			BANSHEE_PATH=/repo/banshee-target/debug/banshee \
			BANSHEE_CFG=/repo/snitch/sw/banshee/config/snitch_cluster.yaml \
			SNITCH_CMAKE_TOOLCHAIN=/repo/cmake/my-toolchain-llvm-docker-banshee.cmake \
			python samples/layernorm.py"


snRuntime-build/common.ld \
snRuntime-build/libsnRuntime.a \
snRuntime-build/libsnRuntime-banshee.a \
snRuntime-build/libsnRuntime-cluster.a \
snRuntime-build/_timestamp \
: _dockerimg banshee-target/debug/banshee
	$(DOCKER_RUN) \
		--mount=type=bind,source=`pwd`,destination=/repo \
		-w /repo \
		spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest \
		/bin/bash -c "\
			mkdir -p /repo/snRuntime-build && \
			cd /repo/snRuntime-build && \
			cmake /repo/snitch/sw/snRuntime \
				-DSNITCH_BANSHEE=/repo/banshee-target/debug/banshee \
				-DSNITCH_SIMULATOR=/repo/snitch_cluster.vlt \
				-DBUILD_TESTS=ON \
				-DSNITCH_RUNTIME=snRuntime-cluster \
				-DCMAKE_TOOLCHAIN_FILE=/repo/cmake/my-toolchain-llvm-docker.cmake && \
			cmake --build . -j `nproc` && \
			touch _timestamp"


run-axpy: _dockerimg snRuntime-build/_timestamp snitch_cluster.vlt banshee-target/debug/banshee
	$(DOCKER_RUN) \
		--mount=type=bind,source=`pwd`,destination=/repo \
		--mount=type=bind,source=`pwd`/dace,destination=/tools/dace \
		--mount=type=bind,source=`pwd`/daceml,destination=/tools/daceml \
		-w /repo \
		spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest \
		/bin/bash -c "\
			RUST_MIN_STACK=134217728 \
			SNITCH_LOG= \
			SNVLT_PATH=/repo/snitch_cluster.vlt \
			BANSHEE_PATH=/repo/banshee-target/debug/banshee \
			BANSHEE_CFG=/repo/snitch/sw/banshee/config/snitch_cluster.yaml \
			SNITCH_CMAKE_TOOLCHAIN=/repo/cmake/my-toolchain-llvm-docker-banshee.cmake \
			python /repo/dace/samples/snitch/axpy.py --simulator=banshee -N 35"


.PHONY: run-npb-gemm-dace
run-npb-gemm-dace \
dace_sdfgs/npbench.benchmarks.polybench.gemm.gemm_dace-kernel.sdfg: _dockerimg snRuntime-build/_timestamp snitch_cluster.vlt banshee-target/debug/banshee
	$(DOCKER_RUN) \
		--mount=type=bind,source=`pwd`,destination=/repo \
		--mount=type=bind,source=`pwd`/dace,destination=/tools/dace \
		--mount=type=bind,source=`pwd`/npbench,destination=/tools/npbench \
		-w /repo \
		spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest \
		/bin/bash -c "\
			python npbench/run_benchmark.py -b gemm -f dace_cpu -p S --save-strict-sdfg true"


.PHONY: run-npb-gemm-snitch
run-npb-gemm-snitch: dace_sdfgs/npbench.benchmarks.polybench.gemm.gemm_dace-kernel.sdfg banshee-target/debug/banshee
	$(DOCKER_RUN) \
		--mount=type=bind,source=`pwd`,destination=/repo \
		--mount=type=bind,source=`pwd`/dace,destination=/tools/dace \
		--mount=type=bind,source=`pwd`/npbench,destination=/tools/npbench \
		-w /repo \
		spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest \
		/bin/bash -c "\
			RUST_MIN_STACK=134217728 \
			SNITCH_LOG= \
			SNVLT_PATH=/repo/snitch_cluster.vlt \
			BANSHEE_PATH=/repo/banshee-target/debug/banshee \
			BANSHEE_CFG=/repo/snitch/sw/banshee/config/snitch_cluster.yaml \
			SNITCH_CMAKE_TOOLCHAIN=/repo/cmake/my-toolchain-llvm-docker-banshee.cmake \
			python -m pdb -c c samples/run_sdfg_snitch.py"


.PHONY: test
test: run-axpy run-transformer run-mlp_simple run-npb-gemm-snitch


dnn_kernels/build-cluster/libDNNKernels.a: snRuntime-build/common.ld
	$(DOCKER_RUN) \
		--mount=type=bind,source=`pwd`,destination=/repo \
		--mount=type=bind,source=`pwd`/dace,destination=/tools/dace \
		--mount=type=bind,source=`pwd`/npbench,destination=/tools/npbench \
		-w /repo \
		spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest \
		/bin/bash -c "\
			export CC=/tools/riscv-llvm-install/bin/clang && \
			export CFLAGS=\" \
				-g -O3 \
				-mcpu=snitch -mcmodel=medany -ffast-math \
				-flto \
				-fno-builtin-printf -fno-common -ffunction-sections \
				-static -mllvm -enable-misched=false -mno-relax \
				-fopenmp -menable-experimental-extensions \
				-isystem /tools/riscv-gnu-install/riscv32-unknown-elf/include/c++/12.2.0/riscv32-unknown-elf/ \
				-isystem /tools/riscv-gnu-install/riscv32-unknown-elf/include/c++/12.2.0/ \
				-isystem /tools/newlib-install/riscv32-unknown-elf/include/ \
				-isystem /tools/riscv-gnu-install/lib/gcc/riscv32-unknown-elf/12.2.0/include \
				-isystem /repo/snitch/sw/snRuntime/include/ \
				-isystem /repo/snitch/sw/vendor/riscv-opcodes/ \
				-isystem /tools/libffi-install/include \
				\" && \
			export AR=/tools/riscv-llvm-install/bin/llvm-ar && \
			export RANLIB=/tools/riscv-llvm-install/bin/llvm-ranlib && \
			cd /repo/dnn_kernels && rm -rf build-banshee build-cluster && mkdir build-banshee && mkdir build-cluster && \
			export LDFLAGS=\" \
				-flto \
				-mcpu=snitch -nostartfiles -fuse-ld=lld -Wl,--image-base=0x80000000 \
				-nostdlib \
				-static \
				-Wl,-z,norelro \
				-Wl,--gc-sections \
				-Wl,--no-relax \
				-nodefaultlibs \
				-T /repo/snRuntime-build/common.ld \
				/tools/riscv-gnu-install/riscv32-unknown-elf/lib/libstdc++.a \
				/tools/newlib-install/riscv32-unknown-elf/lib/libc.a \
				/tools/newlib-install/riscv32-unknown-elf/lib/libm.a \
				/tools/riscv-gnu-install/lib/gcc/riscv32-unknown-elf/12.2.0/libgcc.a \
				/tools/libffi-install/lib/libffi.a \
				/repo/snRuntime-build/libsnRuntime-cluster.a \
				\" && \
			export OBJDUMP=\"/tools/riscv-llvm-install/bin/llvm-objdump --mcpu=snitch\" && \
			BUILD_DIR=build-cluster make build-cluster/libDNNKernels.a && \
			export LDFLAGS=\" \
				-flto \
				-mcpu=snitch -nostartfiles -fuse-ld=lld -Wl,--image-base=0x80000000 \
				-nostdlib \
				-static \
				-Wl,-z,norelro \
				-Wl,--gc-sections \
				-Wl,--no-relax \
				-nodefaultlibs \
				-T /repo/snRuntime-build/common.ld \
				/tools/riscv-gnu-install/riscv32-unknown-elf/lib/libstdc++.a \
				/tools/newlib-install/riscv32-unknown-elf/lib/libc.a \
				/tools/newlib-install/riscv32-unknown-elf/lib/libm.a \
				/tools/riscv-gnu-install/lib/gcc/riscv32-unknown-elf/12.2.0/libgcc.a \
				/tools/libffi-install/lib/libffi.a \
				/repo/snRuntime-build/libsnRuntime-banshee.a \
				\" && \
			BUILD_DIR=build-banshee make build-banshee/libDNNKernels.a"


dnn_kernels/build-cluster/%: snRuntime-build/common.ld
	$(DOCKER_RUN) \
		--mount=type=bind,source=`pwd`,destination=/repo \
		--mount=type=bind,source=`pwd`/dace,destination=/tools/dace \
		--mount=type=bind,source=`pwd`/npbench,destination=/tools/npbench \
		-w /repo \
		spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest \
		/bin/bash -c "\
			export CC=/tools/riscv-llvm-install/bin/clang && \
			export CFLAGS=\" \
				-g -O3 \
				-mcpu=snitch -mcmodel=medany -ffast-math \
				-flto \
				-fno-builtin-printf -fno-common -ffunction-sections \
				-static -mllvm -enable-misched=false -mno-relax \
				-fopenmp -menable-experimental-extensions \
				-isystem /tools/riscv-gnu-install/riscv32-unknown-elf/include/c++/12.2.0/riscv32-unknown-elf/ \
				-isystem /tools/riscv-gnu-install/riscv32-unknown-elf/include/c++/12.2.0/ \
				-isystem /tools/newlib-install/riscv32-unknown-elf/include/ \
				-isystem /tools/riscv-gnu-install/lib/gcc/riscv32-unknown-elf/12.2.0/include \
				-isystem /repo/snitch/sw/snRuntime/include/ \
				-isystem /repo/snitch/sw/snRuntime/vendor/ \
				-isystem /repo/snitch/sw/vendor/riscv-opcodes/ \
				-isystem /tools/libffi-install/include \
				\" && \
			export AR=/tools/riscv-llvm-install/bin/llvm-ar && \
			export RANLIB=/tools/riscv-llvm-install/bin/llvm-ranlib && \
			cd /repo/dnn_kernels && rm -rf build-banshee build-cluster && mkdir build-banshee && mkdir build-cluster && \
			export LDFLAGS=\" \
				-flto \
				-mcpu=snitch -nostartfiles -fuse-ld=lld -Wl,--image-base=0x80000000 \
				-nostdlib \
				-static \
				-Wl,-z,norelro \
				-Wl,--gc-sections \
				-Wl,--no-relax \
				-nodefaultlibs \
				-T /repo/snRuntime-build/common.ld \
				/tools/riscv-gnu-install/riscv32-unknown-elf/lib/libstdc++.a \
				/tools/newlib-install/riscv32-unknown-elf/lib/libc.a \
				/tools/newlib-install/riscv32-unknown-elf/lib/libm.a \
				/tools/riscv-gnu-install/lib/gcc/riscv32-unknown-elf/12.2.0/libgcc.a \
				/tools/libffi-install/lib/libffi.a \
				/repo/snRuntime-build/libsnRuntime-cluster.a \
				\" && \
			export OBJDUMP=\"/tools/riscv-llvm-install/bin/llvm-objdump --mcpu=snitch\" && \
			BUILD_DIR=build-cluster make build-cluster/$(@:dnn_kernels/build-cluster/%=%) build-cluster/$(@:dnn_kernels/build-cluster/%=%.s) && \
			export LDFLAGS=\" \
				-flto \
				-mcpu=snitch -nostartfiles -fuse-ld=lld -Wl,--image-base=0x80000000 \
				-nostdlib \
				-static \
				-Wl,-z,norelro \
				-Wl,--gc-sections \
				-Wl,--no-relax \
				-nodefaultlibs \
				-T /repo/snRuntime-build/common.ld \
				/tools/riscv-gnu-install/riscv32-unknown-elf/lib/libstdc++.a \
				/tools/newlib-install/riscv32-unknown-elf/lib/libc.a \
				/tools/newlib-install/riscv32-unknown-elf/lib/libm.a \
				/tools/riscv-gnu-install/lib/gcc/riscv32-unknown-elf/12.2.0/libgcc.a \
				/tools/libffi-install/lib/libffi.a \
				/repo/snRuntime-build/libsnRuntime-banshee.a \
				\" && \
			BUILD_DIR=build-banshee make build-banshee/$(@:dnn_kernels/build-cluster/%=%) build-banshee/$(@:dnn_kernels/build-cluster/%=%.s)"


# make verilator-dnn-abs-10-bench
verilator-dnn-%: dnn_kernels/build-cluster/% snitch_cluster.vlt
	$(DOCKER_RUN) \
		--mount=type=bind,source=`pwd`,destination=/repo \
		--mount=type=bind,source=`pwd`/dace,destination=/tools/dace \
		--mount=type=bind,source=`pwd`/npbench,destination=/tools/npbench \
		-w /repo \
		spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest \
		/bin/bash -c "\
			./snitch_cluster.vlt \
			/repo/dnn_kernels/build-cluster/$(@:verilator-dnn-%=%)"


# make banshee-dnn-abs-10-bench
banshee-dnn-%: dnn_kernels/build-cluster/% banshee-target/debug/banshee
	$(DOCKER_RUN) \
		--mount=type=bind,source=`pwd`,destination=/repo \
		--mount=type=bind,source=`pwd`/dace,destination=/tools/dace \
		--mount=type=bind,source=`pwd`/npbench,destination=/tools/npbench \
		-w /repo \
		spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest \
		/bin/bash -c "\
			RUST_MIN_STACK=134217728 \
			SNITCH_LOG= \
			/repo/banshee-target/debug/banshee \
				--configuration /repo/snitch/sw/banshee/config/snitch_cluster.yaml \
				--latency \
				/repo/dnn_kernels/build-banshee/$(@:banshee-dnn-%=%)"

build-snitch-gemm:
	$(DOCKER_RUN) \
		--mount=type=bind,source=`pwd`,destination=/repo \
		-w /repo \
		ghcr.io/pulp-platform/snitch \
		/bin/bash -c "\
			cd /repo/snitch/sw/applications && \
			rm -rf build && mkdir build && cd build && \
			cmake -DCMAKE_TOOLCHAIN_FILE=/repo/snitch/sw/cmake/toolchain-llvm.cmake \
				-DSNITCH_RUNTIME=snRuntime-banshee \
				.. && \
			cmake --build . "
		
run-snitch-gemm:
	$(DOCKER_RUN) \
		--mount=type=bind,source=`pwd`,destination=/repo \
		-w /repo \
		spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest \
		/bin/bash -c "\
			cd /repo/snitch/sw/applications/build && \
			/repo/banshee-target/debug/banshee \
			--configuration /repo/snitch/sw/banshee/config/snitch_cluster.yaml \
			--latency \
			./gemm \
		"

build-vlt-snitch-gemm:
	$(DOCKER_RUN) \
		--mount=type=bind,source=`pwd`,destination=/repo \
		-w /repo \
		ghcr.io/pulp-platform/snitch \
		/bin/bash -c "\
			cd /repo/snitch/sw/applications && \
			rm -rf build && mkdir build && cd build && \
			cmake -DCMAKE_TOOLCHAIN_FILE=/repo/snitch/sw/cmake/toolchain-llvm.cmake \
				-DSNITCH_RUNTIME=snRuntime-cluster \
				.. && \
			cmake --build . "
		
run-vlt-snitch-gemm:
	$(DOCKER_RUN) \
		--mount=type=bind,source=`pwd`,destination=/repo \
		-w /repo \
		spclgitlab.ethz.ch:5050/anivanov/daceml-snitch:latest \
		/bin/bash -c "\
			cd /repo/snitch/sw/applications/build && \
			/repo/snitch_cluster.vlt \
			./gemm \
		"

#		rm -rf /code/llvm-project/build && \


build-my-clang-12:
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`/..,destination=/code -w / ghcr.io/pulp-platform/snitch /bin/bash -c "\
		mkdir -p /code/llvm-project/build && \
		cd /code/llvm-project/build && \
		cmake /code/llvm-project/llvm \
			-DCMAKE_BUILD_TYPE=\"Release\" \
		    -DLLVM_ENABLE_PROJECTS=\"clang;lld\" \
			-DLLVM_TARGETS_TO_BUILD=\"RISCV\" \
			-DLLVM_DEFAULT_TARGET_TRIPLE=\"riscv32-unknown-elf\" \
			-DLLVM_OPTIMIZED_TABLEGEN=True \
			-DCMAKE_INSTALL_PREFIX=/code/snitch-llvm-12 && \
		cmake --build . -j `nproc` && \
		cmake --install ."

#		rm -rf /code/llvm-project-paul/build && \


build-my-clang-paul:
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`/..,destination=/code -w / ghcr.io/pulp-platform/snitch /bin/bash -c "\
		mkdir -p /code/llvm-project-paul/build && \
		cd /code/llvm-project-paul/build && \
		cmake /code/llvm-project-paul/llvm \
			-DCMAKE_BUILD_TYPE=\"Release\" -DLLVM_ENABLE_ASSERTIONS=ON \
		    -DLLVM_ENABLE_PROJECTS=\"clang;lld\" \
			-DLLVM_TARGETS_TO_BUILD=\"RISCV\" \
			-DLLVM_DEFAULT_TARGET_TRIPLE=\"riscv32-unknown-elf\" \
			-DLLVM_OPTIMIZED_TABLEGEN=True \
    		-DLLVM_ENABLE_LLD=True -DLLVM_APPEND_VC_REV=OFF \
			-DCMAKE_INSTALL_PREFIX=/code/snitch-llvm-paul && \
		cmake --build . -j `nproc` && \
		cmake --install ."


build-my-compiler-rt-paul:
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`/..,destination=/code -w / ghcr.io/pulp-platform/snitch /bin/bash -c "\
		mkdir -p /code/llvm-project-paul/build-compiler-rt && \
		cd /code/llvm-project-paul/build-compiler-rt && \
		cmake ../compiler-rt \
			-DCMAKE_SYSTEM_NAME=Linux \
			-DCMAKE_INSTALL_PREFIX=/code/snitch-llvm-paul/lib/clang/15.0.0 \
			-DCMAKE_C_COMPILER=/code/snitch-llvm-paul/bin/clang \
			-DCMAKE_CXX_COMPILER=/code/snitch-llvm-paul/bin/clang \
			-DCMAKE_ASM_COMPILER=/code/snitch-llvm-paul/bin/clang \
			-DCMAKE_AR=/code/snitch-llvm-paul/bin/llvm-ar \
			-DCMAKE_NM=/code/snitch-llvm-paul/bin/llvm-nm \
			-DCMAKE_RANLIB=/code/snitch-llvm-paul/bin/llvm-ranlib \
			-DCMAKE_C_COMPILER_TARGET=riscv32-unknown-elf \
			-DCMAKE_CXX_COMPILER_TARGET=riscv32-unknown-elf \
			-DCMAKE_ASM_COMPILER_TARGET=riscv32-unknown-elf \
			-DCMAKE_C_FLAGS='-march=rv32imafd -mabi=ilp32d' \
			-DCMAKE_CXX_FLAGS='-march=rv32imafd -mabi=ilp32d' \
			-DCMAKE_ASM_FLAGS='-march=rv32imafd -mabi=ilp32d' \
			-DCMAKE_EXE_LINKER_FLAGS='-nostartfiles -nostdlib -fuse-ld=lld' \
			-DCOMPILER_RT_BAREMETAL_BUILD=ON \
			-DCOMPILER_RT_BUILD_BUILTINS=ON \
			-DCOMPILER_RT_BUILD_MEMPROF=OFF \
			-DCOMPILER_RT_BUILD_LIBFUZZER=OFF \
			-DCOMPILER_RT_BUILD_PROFILE=OFF \
			-DCOMPILER_RT_BUILD_SANITIZERS=OFF \
			-DCOMPILER_RT_BUILD_XRAY=OFF \
			-DCOMPILER_RT_DEFAULT_TARGET_ONLY=ON \
			-DCOMPILER_RT_OS_DIR='' \
			-DLLVM_CONFIG_PATH=/code/snitch-llvm-paul/bin/llvm-config && \
		cmake --build . -j `nproc` && \
		cmake --install ."


# 
build-my-newlib-paul:
	$(DOCKER_RUN) --mount=type=bind,source=`pwd`/..,destination=/code -w / ghcr.io/pulp-platform/snitch /bin/bash -c "\
		mkdir -p /code/newlib/build && \
		cd /code/newlib/build && \
		/code/newlib/configure \
			--target=riscv32-unknown-elf \
			--prefix=/code/snitch-llvm-paul \
			AR_FOR_TARGET=/tools/riscv-llvm/bin/llvm-ar \
			AS_FOR_TARGET=/tools/riscv-llvm/bin/llvm-as \
			LD_FOR_TARGET=/tools/riscv-llvm/bin/llvm-ld \
			RANLIB_FOR_TARGET=/tools/riscv-llvm/bin/llvm-ranlib \
			CC_FOR_TARGET=/tools/riscv-llvm/bin/clang \
			CFLAGS_FOR_TARGET=\"-march=rv32imafd -mabi=ilp32d -O2\" && \
		make && make install"



